use std::{
    cmp::{
        max,
        min,
    },
    collections::{
        HashSet,
        VecDeque,
    },
    hash::{
        Hash,
    },
    num::ParseIntError,
};
use thiserror::Error;


#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error("Invalid argument: {0}")]
    InvalidArgument(#[from] ParseIntError),
}

#[derive(Default, Debug)]
pub struct Game {
    decks: [VecDeque<usize>; 2],
}

impl Game {
    pub fn from_lines(lines: &mut impl Iterator<Item = String>) -> Result<Self, Error> {
        let mut game: Self = Default::default();
        let mut player = game.decks.len();  // Initialise with invalid player
        for line in lines {
            match line.strip_prefix("Player ") {
                Some(player_string) => player = player_string.strip_suffix(":").unwrap().parse::<usize>()? - 1,
                None => match line.parse() {
                    Ok(card) => game.decks[player].push_back(card),
                    Err(_) => {},
                },
            }
        }
        Ok(game)
    }

    pub fn play_turn(&mut self) -> bool {
        let a = self.decks[0].pop_front().unwrap();
        let b = self.decks[1].pop_front().unwrap();
        let winner_deck = &mut self.decks[(a < b) as usize];
        winner_deck.push_back(max(a, b));
        winner_deck.push_back(min(a, b));
        !self.decks[(b < a) as usize].is_empty()
    }

    pub fn score(&self) -> usize {
        let winner_deck = &self.decks[(self.decks[0].len() < self.decks[1].len()) as usize];
        let mut accumulator = 0;
        for (card, multiplier) in winner_deck.iter().rev().zip(1..) {
            accumulator += *card * multiplier;
        }
        accumulator
    }
}

#[derive(Clone, Default, Debug, Eq, Hash, PartialEq)]
struct GameState {
    decks: [VecDeque<usize>; 2],
}

#[derive(Default, Debug)]
pub struct RecursiveGame {
    state: GameState,
    state_history: HashSet<GameState>,
}

impl RecursiveGame {
    pub fn from_lines(lines: &mut impl Iterator<Item = String>) -> Result<Self, Error> {
        let mut game: Self = Default::default();
        let mut player = game.state.decks.len();  // Initialise with invalid player
        for line in lines {
            match line.strip_prefix("Player ") {
                Some(player_string) => player = player_string.strip_suffix(":").unwrap().parse::<usize>()? - 1,
                None => match line.parse() {
                    Ok(card) => game.state.decks[player].push_back(card),
                    Err(_) => {},
                },
            }
        }
        Ok(game)
    }

    fn play_turn(&mut self) -> Option<usize> {
        if !self.state_history.insert(self.state.clone()) {
            Some(0)
        } else {
            let cards = [
                self.state.decks[0].pop_front().unwrap(),
                self.state.decks[1].pop_front().unwrap(),
            ];
            let winner = if cards[0] <= self.state.decks[0].len() && cards[1] <= self.state.decks[1].len() {
                let mut sub_game = RecursiveGame{
                    state: GameState{
                        decks: [
                            self.state.decks[0].iter().take(cards[0]).cloned().collect(),
                            self.state.decks[1].iter().take(cards[1]).cloned().collect(),
                        ],
                    },
                    state_history: Default::default(),
                };
                sub_game.play_to_finish()
            } else {
                (cards[0] < cards[1]) as usize
            };
            let loser = !(winner > 0) as usize;
            self.state.decks[winner].push_back(cards[winner]);
            self.state.decks[winner].push_back(cards[!(winner > 0) as usize]);
            if self.state.decks[loser].len() == 0 {
                Some(winner)
            } else {
                None
            }
        }
    }

    pub fn play_to_finish(&mut self) -> usize {
        loop {
            match self.play_turn() {
                Some(player) => return player,
                None => {},
            }
        }
    }

    pub fn score(&self, player: usize) -> usize {
        let winner_deck = &self.state.decks[player];
        let mut accumulator = 0;
        for (card, multiplier) in winner_deck.iter().rev().zip(1..) {
            accumulator += *card * multiplier;
        }
        accumulator
    }
}
