use std::collections::BTreeMap;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum CustomsError {
    #[error("Unknown Question: {q:?}")]
    UnknownQuestion {
        q: char,
    },
}

#[derive(Default)]
pub struct CustomsDeclaration {
    members: usize,
    questions: BTreeMap<char, usize>,
}

impl CustomsDeclaration {
    pub fn from_lines(lines: &mut impl std::iter::Iterator<Item = std::io::Result<String>>) -> Result<Self, CustomsError> {
        let mut cd = Default::default();

        loop {
            match lines.next() {
                Some(line) => match line.unwrap().as_str() {
                    "" => return Ok(cd),
                    s => {
                        cd.members += 1;
                        for c in s.chars() {
                            match cd.questions.get_mut(&c) {
                                Some(v) => *v += 1,
                                None => {
                                    cd.questions.insert(c, 1);
                                },
                            }
                        }
                    },
                },
                None => return Ok(cd),
            }
        }
    }

    pub fn all_count(&self) -> usize {
        let mut counter = 0;
        for v in self.questions.values() {
            if v == &self.members {
                counter += 1
            }
        }
        counter
    } 

    pub fn any_count(&self) -> usize {
        self.questions.len()
    } 
}
