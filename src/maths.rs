use thiserror::Error;
use pest::{
    Parser,
    iterators::Pairs,
};
use pest_derive::Parser;
use std::num::ParseIntError;

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    ParseError(#[from] pest::error::Error<Rule>),

    #[error("Invalid argument: {0}")]
    InvalidArgument(#[from] ParseIntError),

    #[error("Invalid Operand {0:?}: {1}")]
    InvalidOperand(Rule, String),

    #[error("Invalid Operation {0:?}: {1}")]
    InvalidOperation(Rule, String),

    #[error("Missing Operand")]
    MissingOperand,
}

#[derive(Parser)]
#[grammar_inline = r#"
WHITESPACE = _{ " " }

np_expression = _{ np_operand ~ ( np_operation ~ np_operand )* }
np_operation = _{ add | multiply }
np_operand = _{ number | np_bracketed }
np_bracketed = _{ "(" ~ np_expression ~ ")" }

af_expression = _{ af_addition ~ ( multiply ~ af_addition )* }
af_addition = { af_operand ~ ( add ~ af_operand )* }
af_operand = _{ number | af_bracketed }
af_bracketed = { "(" ~ af_expression ~ ")" }

add = { "+" }
multiply = { "*" }
number = @{ NUMBER+ }
"#]
struct MathsParser;

fn evaluate_operand(pairs: &mut Pairs<Rule>) -> Result<usize, Error> {
    Ok(match pairs.next() {
        Some(pair) => match pair.as_rule() {
            Rule::number => pair.as_span().as_str().parse::<usize>().unwrap(),
            Rule::af_addition => execute_expression_pairs(pair.into_inner())?,
            Rule::af_bracketed => execute_expression_pairs(pair.into_inner())?,
            Rule::np_bracketed => execute_expression_pairs(pair.into_inner())?,
            _ => return Err(Error::InvalidOperand(pair.as_rule(), pair.as_span().as_str().to_string())),
        },
        None => return Err(Error::MissingOperand),
    })
}

fn execute_expression_pairs(mut pairs: Pairs<Rule>) -> Result<usize, Error> {
    let mut result = evaluate_operand(&mut pairs)?;
    while let Some(operation_pair) = pairs.next() {
        let operand_value = evaluate_operand(&mut pairs)?;
        match operation_pair.as_rule() {
            Rule::add => result += operand_value,
            Rule::multiply => result *= operand_value,
            _ => return Err(Error::InvalidOperation(operation_pair.as_rule(), operation_pair.as_span().as_str().to_string())),
        }
    }
    Ok(result)
}

pub fn execute_line_np(line: &str) -> Result<usize, Error> {
    let pairs = MathsParser::parse(Rule::np_expression, line)?;
    execute_expression_pairs(pairs)
}

pub fn execute_line_af(line: &str) -> Result<usize, Error> {
    let pairs = MathsParser::parse(Rule::af_expression, line)?;
    execute_expression_pairs(pairs)
}
