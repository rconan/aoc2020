use thiserror::Error;
use std::{
    collections::{
        HashMap,
        HashSet,
    },
    num::ParseIntError,
};

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error("Invalid argument: {0}")]
    InvalidArgument(#[from] ParseIntError),
}

pub struct Image {
    pixels: Vec<Vec<bool>>,
}

impl Image {
    fn from_lines(lines: &mut impl Iterator<Item = String>) -> Result<Self, Error> {
        let mut pixels: Vec<Vec<bool>> = Default::default();
        for line in lines {
            if line == "" {
                break
            }
            pixels.push(line.chars().map(|c| match c {
                '#' => true,
                '.' => false,
                c => panic!("Unexpected character {}", c),
            }).collect());
        }
        Ok(Self{
            pixels: pixels,
        })
    }

    fn copy(&self) -> Image {
        Image{
            pixels: self.pixels.iter().cloned().collect(),
        }
    }

    fn mirror_horizontal(&self) -> Image {
        Image{
            pixels: self.pixels.iter().rev().cloned().collect(),
        }
    }

    fn mirror_vertical(&self) -> Image {
        let mut new_pixels: Vec<Vec<bool>> = Default::default();
        for row in self.pixels.iter() {
            new_pixels.push(row.iter().rev().cloned().collect());
        }
        Image{
            pixels: new_pixels,
        }
    }

    fn mirror_backwards_diagonal(&self) -> Image {
        let mut new_pixels: Vec<Vec<bool>> = vec![Default::default(); self.pixels.len()];
        for row in self.pixels.iter() {
            for (i, pixel) in row.iter().enumerate() {
                new_pixels[i].push(*pixel);
            }
        }
        Image{
            pixels: new_pixels,
        }
    }

    fn mirror_forwards_diagonal(&self) -> Image {
        let mut new_pixels: Vec<Vec<bool>> = vec![Default::default(); self.pixels.len()];
        for row in self.pixels.iter().rev() {
            for (i, pixel) in row.iter().rev().enumerate() {
                new_pixels[i].push(*pixel);
            }
        }
        Image{
            pixels: new_pixels,
        }
    }

    fn rotate_quarter_anticlockwise(&self) -> Image {
        let mut new_pixels: Vec<Vec<bool>> = vec![Default::default(); self.pixels.len()];
        for row in self.pixels.iter() {
            for (i, pixel) in row.iter().rev().enumerate() {
                new_pixels[i].push(*pixel);
            }
        }
        Image{
            pixels: new_pixels,
        }
    }

    fn rotate_quarter_clockwise(&self) -> Image {
        let mut new_pixels: Vec<Vec<bool>> = vec![Default::default(); self.pixels.len()];
        for row in self.pixels.iter().rev() {
            for (i, pixel) in row.iter().enumerate() {
                new_pixels[i].push(*pixel);
            }
        }
        Image{
            pixels: new_pixels,
        }
    }

    fn rotate_half(&self) -> Image {
        let mut new_pixels: Vec<Vec<bool>> = Default::default();
        for row in self.pixels.iter().rev() {
            new_pixels.push(row.iter().rev().cloned().collect());
        }
        Image{
            pixels: new_pixels,
        }
    }

    fn top_edge(&self) -> Vec<bool> {
        self.pixels.iter().next().unwrap().iter().cloned().collect()
    }

    fn bottom_edge(&self) -> Vec<bool> {
        self.pixels.iter().next_back().unwrap().iter().cloned().collect()
    }

    fn left_edge(&self) -> Vec<bool> {
        self.pixels.iter().map(|r| r.iter().next().unwrap()).cloned().collect()
    }

    fn right_edge(&self) -> Vec<bool> {
        self.pixels.iter().map(|r| r.iter().next_back().unwrap()).cloned().collect()
    }

    fn reverse_top_edge(&self) -> Vec<bool> {
        self.top_edge().iter().rev().cloned().collect()
    }

    fn reverse_bottom_edge(&self) -> Vec<bool> {
        self.bottom_edge().iter().rev().cloned().collect()
    }

    fn reverse_left_edge(&self) -> Vec<bool> {
        self.left_edge().iter().rev().cloned().collect()
    }

    fn reverse_right_edge(&self) -> Vec<bool> {
        self.right_edge().iter().rev().cloned().collect()
    }

    fn edges(&self) -> [Vec<bool>; 8] {
        [
            self.top_edge(),
            self.bottom_edge(),
            self.left_edge(),
            self.right_edge(),
            self.reverse_top_edge(),
            self.reverse_bottom_edge(),
            self.reverse_left_edge(),
            self.reverse_right_edge(),
        ]
    }

    pub fn edge_to_left(&self, edge: usize) -> Image {
        match edge {
            0 => self.mirror_backwards_diagonal(),
            1 => self.rotate_quarter_clockwise(),
            2 => self.copy(),
            3 => self.mirror_vertical(),
            4 => self.rotate_quarter_anticlockwise(),
            5 => self.mirror_forwards_diagonal(),
            6 => self.mirror_horizontal(),
            7 => self.rotate_half(),
            n => panic!("Not an edge: {}", n),
        }
    }

    fn crop(&self) -> Vec<&[bool]> {
        let mut iter = self.pixels.iter();
        iter.next();
        iter.next_back();
        let mut result: Vec<&[bool]> = Default::default();
        for row in iter {
            result.push(&row[1..row.len() - 1]);
        }
        result
    }

    pub fn print(&self) {
        for row in &self.pixels {
            for pixel in row {
                print!("{}", if *pixel { '#' } else { '.' });
            }
            println!("");
        }
        println!("");
    }

    pub fn count_pixels(&self) -> usize {
        let mut accumulator = 0;
        for row in &self.pixels {
            for pixel in row {
                if *pixel {
                    accumulator += 1;
                }
            }
        }
        accumulator
    }

    pub fn count_monsters(&self) -> usize {
        let mut accumulator = 0;
        for (j, row) in self.pixels[..self.pixels.len() - 3].iter().enumerate() {
            'search: for i in 0..row.len() - 20 {
                for (di, dj) in &[(18, 0), (0, 1), (5, 1), (6, 1), (11, 1), (12, 1), (17, 1), (18, 1), (19, 1), (1, 2), (4, 2), (7, 2), (10, 2), (13, 2), (16, 2)] {
                    if !self.pixels[j + dj][i + di] {
                        continue 'search;
                    }
                }
                accumulator += 1;
            }
        }
        accumulator
    }
}

pub struct Array {
    edge_map: HashMap<Vec<bool>, Vec<(usize, usize)>>,
    images: HashMap<usize, Image>,
}

impl Array {
    pub fn from_lines(lines: &mut impl Iterator<Item = std::io::Result<String>>) -> Result<Self, Error> {
        let mut input = lines.map(|l| l.unwrap());
        let mut images: HashMap<usize, Image> = Default::default();
        while let Some(line) = input.next() {
            let id: usize = line.strip_prefix("Tile ").unwrap().strip_suffix(":").unwrap().parse()?;
            let image = Image::from_lines(&mut input)?;
            images.insert(id, image);
        }
        let edge_map = Self::edge_map(images.iter());
        Ok(Array{
            edge_map: edge_map,
            images: images,
        })
    }

    fn edge_map<'a>(images: impl Iterator<Item = (&'a usize, &'a Image)>) -> HashMap<Vec<bool>, Vec<(usize, usize)>> {
        let mut edge_map: HashMap<Vec<bool>, Vec<(usize, usize)>> = Default::default();
        for (id, image) in images {
            for (i, edge) in image.edges().iter().enumerate() {
                match edge_map.get_mut(edge) {
                    Some(v) => { 
                        v.push((*id, i))
                    },
                    None => { edge_map.insert(edge.iter().cloned().collect(), vec![(*id, i)]); },
                }
            }
        }
        edge_map
    }

    fn candidate_corner<'a>(&self, images: &mut impl Iterator<Item = (&'a usize, &'a Image)>) -> Option<usize> {
        for (id, image) in images {
            let mut matching_edge_count = 0;
            for edge in &image.edges() {
                match self.edge_map.get(edge) {
                    Some(v) => { if v.len() > 1 { matching_edge_count += 1 } },
                    None => panic!("Edge not recorded in edge map"),
                }
            }
            if matching_edge_count == 4 { // 4 matching means 2 candidates due to reversal
                return Some(*id)
            }
        }
        None
    }

    pub fn candidate_corner_tiles(&self) -> Vec<usize> {
        let mut candidates: Vec<usize> = Default::default();
        let mut iter = self.images.iter();
        while let Some(candidate) = self.candidate_corner(&mut iter) {
            candidates.push(candidate);
        }
        candidates
    }

    pub fn full_image(&self) -> Image {
        let mut tiles: HashSet<usize> = self.images.keys().cloned().collect();
        let mut current_tile = self.candidate_corner(&mut self.images.iter()).unwrap();
        let mut current_orientation = 2; // Left
        let mut current_row = 0;
        let mut image: Vec<Vec<bool>> = Default::default();
        loop {
            let current_image = &self.images[&current_tile].edge_to_left(current_orientation);
            let first_tile = current_tile;
            let first_tile_bottom_edge = current_image.bottom_edge();
            loop {
                if !tiles.remove(&current_tile) {
                    panic!("Tile not available {}", current_tile);
                }
                let current_image = &self.images[&current_tile].edge_to_left(current_orientation);
                Self::add_to_image(&mut image, current_row, current_image.crop());
                let right_edges = &self.edge_map[&current_image.right_edge()];
                match right_edges.len() {
                    0 => panic!("Right edge not in edge map"),
                    1 => break,
                    2 => {
                        let (next_tile, next_orientation) = right_edges.iter().filter(|(t, _)| *t != current_tile).next().unwrap();
                        current_tile = *next_tile;
                        current_orientation = *next_orientation;
                    },
                    n => panic!("Right edge in edge map {} times", n),
                }
            }
            let bottom_edges = &self.edge_map[&first_tile_bottom_edge];
            match bottom_edges.len() {
                0 => panic!("Bottom edge not in edge map"),
                1 => break,
                2 => {
                    let (next_tile, next_orientation) = bottom_edges.iter().filter(|(t, _)| *t != first_tile).next().unwrap();
                    current_tile = *next_tile;
                    current_orientation = match *next_orientation {
                        0 => 2,
                        1 => 6,
                        2 => 0,
                        3 => 4,
                        4 => 3,
                        5 => 7,
                        6 => 1,
                        7 => 5,
                        n => panic!("Not an edge type {}", n),
                    };
                    current_row += first_tile_bottom_edge.len() - 2;  // Everything is square, we have this to hand
                },
                n => panic!("Right edge in edge map {} times", n),
            }
        }
        Image{
            pixels: image,
        }
    }

    fn add_to_image(image: &mut Vec<Vec<bool>>, start_row: usize, pixels: Vec<&[bool]>) {
        for (i, row) in pixels.iter().enumerate() {
            match image.get_mut(start_row + i) {
                Some(r) => r.extend(row.iter()),
                // The only scenario this should be None should be when it's the next row
                None => {
                    if start_row + i != image.len() {
                        panic!("Trying to skip rows");
                    }
                    image.push(row.iter().cloned().collect());
                },
            }
        }
    }
}
