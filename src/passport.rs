use thiserror::Error;

#[derive(Error, Debug)]
pub enum PassportError {
    #[error("Unknown Field: {field:?}")]
    UnknownField {
        field: String,
    },
}

#[derive(Default)]
pub struct Passport {
    byr: String,
    iyr: String,
    eyr: String,
    hgt: String,
    hcl: String,
    ecl: String,
    pid: String,
    cid: String,
}

impl Passport {
    pub fn from_lines(lines: &mut impl std::iter::Iterator<Item = std::io::Result<String>>) -> Result<Passport, PassportError> {
        let mut passport = Default::default();

        loop {
            match lines.next() {
                Some(line) => match line.unwrap().as_str() {
                    "" => return Ok(passport),
                    s => {
                        let parts = s.split(" ");
                        for part in parts {
                            let mut kv = part.split(":");
                            match kv.next().unwrap() {
                                "byr" => passport.byr = kv.next().unwrap().to_string(),
                                "iyr" => passport.iyr = kv.next().unwrap().to_string(),
                                "eyr" => passport.eyr = kv.next().unwrap().to_string(),
                                "hgt" => passport.hgt = kv.next().unwrap().to_string(),
                                "hcl" => passport.hcl = kv.next().unwrap().to_string(),
                                "ecl" => passport.ecl = kv.next().unwrap().to_string(),
                                "pid" => passport.pid = kv.next().unwrap().to_string(),
                                "cid" => passport.cid = kv.next().unwrap().to_string(),
                                val => return Err(PassportError::UnknownField{field: val.to_string()})
                            }
                        }
                    }
                },
                None => return Ok(passport),
            }
        }
    }

    pub fn is_complete(&self) -> bool {
        self.byr != "" &&
        self.iyr != "" &&
        self.eyr != "" &&
        self.hgt != "" &&
        self.hcl != "" &&
        self.ecl != "" &&
        self.pid != ""
    }

    fn byr_valid(&self) -> bool {
        match self.byr.as_str().parse::<u32>() {
            Ok(yr) => yr >= 1920 && yr <= 2002,
            Err(_) => false,
        }
    }

    fn iyr_valid(&self) -> bool {
        match self.iyr.as_str().parse::<u32>() {
            Ok(yr) => yr >= 2010 && yr <= 2020,
            Err(_) => false,
        }
    }

    fn eyr_valid(&self) -> bool {
        match self.eyr.as_str().parse::<u32>() {
            Ok(yr) => yr >= 2020 && yr <= 2030,
            Err(_) => false,
        }
    }

    fn hgt_valid(&self) -> bool {
        match self.hgt.strip_suffix("cm") {
            Some(dim) => match dim.parse::<u32>() {
                Ok(h) => h >= 150 && h <= 193,
                Err(_) => false,
            },
            None => match self.hgt.strip_suffix("in") {
                Some(dim) => match dim.parse::<u32>() {
                    Ok(h) => h >= 59 && h <= 76,
                    Err(_) => false,
                },
                None => false,
            }
        }
    }

    fn hcl_valid(&self) -> bool {
        match self.hcl.strip_prefix("#") {
            Some(col) => {
                for c in col.chars() {
                    if c.is_ascii_uppercase() || !c.is_ascii_hexdigit() {
                        return false
                    }
                }
                true
            },
            None => false,
        }
    }

    fn ecl_valid(&self) -> bool {
        match self.ecl.as_str() {
            "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => true,
            &_ => false,
        }
    }

    fn pid_valid(&self) -> bool {
        if self.pid.len() != 9 {
            return false
        }
        for c in self.pid.chars() {
            if !c.is_ascii_digit() {
                return false
            }
        }
        true
    }

    pub fn is_valid(&self) -> bool {
        self.byr_valid() &&
        self.iyr_valid() &&
        self.eyr_valid() &&
        self.hgt_valid() &&
        self.hcl_valid() &&
        self.ecl_valid() &&
        self.pid_valid()
    }
}
