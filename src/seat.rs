use std::{
    cmp::min,
    fmt,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum SeatError {
    #[error("Unknown column modifier {m} at position {p}")]
    UnknownColumnModifier {
        m: char,
        p: usize,
    },
    #[error("Unknown row modifier {m} at position {p}")]
    UnknownRowModifier {
        m: char,
        p: usize,
    },
    #[error("Invalid length {l}")]
    WrongLength {
        l: usize,
    },
}

#[derive(Default)]
pub struct Seat {
    column: u8,
    row: u8,
}

impl Seat {
    pub fn from_string(s: &str) -> Result<Self, SeatError> {
        let mut seat: Self = Default::default();
        let mut d = 64;
        let l = s.len();
        if l != 10 {
            return Err(SeatError::WrongLength{l: l})
        }
        for (p, c) in s[0..7].chars().enumerate() {
            match c {
                'F' => {},
                'B' => seat.row += d,
                m  => return Err(SeatError::UnknownRowModifier{m: m, p: p}),
            }
            d /= 2;
        }
        d = 4;
        for (c, p) in s[7..10].chars().zip(7..10) {
            match c {
                'L' => {},
                'R' => seat.column += d,
                m  => return Err(SeatError::UnknownColumnModifier{m: m, p: p}),
            }
            d /= 2;
        }
        Ok(seat)
    }

    pub fn id(&self) -> u16 {
        self.row as u16 * 8 + self.column as u16
    }
}

#[derive(Error, Debug)]
pub enum SeatLayoutError {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error("Invalid Position: {0}")]
    InvalidPosition(char),
}

#[derive(Debug, Eq, PartialEq)]
pub enum LayoutPosition {
    Floor,
    Empty,
    Occupied,
}

impl fmt::Display for LayoutPosition {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            Self::Floor => ".",
            Self::Empty => "L",
            Self::Occupied => "#",
        })
    }
}

pub struct SeatLayout {
    positions: Vec<Vec<LayoutPosition>>,
}

impl SeatLayout {
    pub fn from_lines(lines: &mut impl std::iter::Iterator<Item = std::io::Result<String>>) -> Result<Self, SeatLayoutError> {
        let mut positions = Vec::new();
        for line in lines {
            let mut row = Vec::new();
            for position in line?.chars() {
                row.push(match position {
                    '.' => LayoutPosition::Floor,
                    'L' => LayoutPosition::Empty,
                    '#' => LayoutPosition::Occupied,
                    c => return Err(SeatLayoutError::InvalidPosition(c)),
                });
            }
            positions.push(row);
        }
        Ok(SeatLayout{
            positions: positions,
        })
    }

    fn seats_adjacent(&self, i: usize, j: usize) -> u8 {
        let mut count = 0;
        for jj in if j > 0 { j - 1 } else { 0 }..min(j + 2, self.positions.len()) {
            for ii in if i > 0 { i - 1 } else { 0 }..min(i + 2, self.positions[j].len()) {
                if !(ii == i && jj == j) && self.positions[jj][ii] == LayoutPosition::Occupied {
                    count += 1;
                }
            }
        }
        count
    }

    fn seats_visible(&self, i: usize, j: usize) -> u8 {
        let mut count = 0;
        for (di, dj) in &[(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)] {
            //println!("Scanning direction {},{}", di, dj);
            let (mut ii, mut jj) = (i, j);
            while
                ii as isize + di >= 0 &&
                ii as isize + di < self.positions[jj].len() as isize &&
                jj as isize + dj >= 0 &&
                jj as isize + dj < self.positions.len() as isize
            {
                ii = (ii as isize + di) as usize;
                jj = (jj as isize + dj) as usize;
                //println!("Seat {},{} is in direction {},{} from {},{} and is {:?}", ii, jj, di, dj, i, j, self.positions[jj][ii]);
                match self.positions[jj][ii] {
                    LayoutPosition::Occupied => {
                        count += 1;
                        break;
                    },
                    LayoutPosition::Empty => break,
                    LayoutPosition::Floor => continue,
                }
            }
        }
        //println!("Seat {},{} has {} visible", i, j, count);
        count
    }

    pub fn iterate_adjacent(&mut self) -> (bool, usize) {
        let mut new_positions: Vec<Vec<LayoutPosition>> = Vec::new();
        let mut changed = false;
        let mut occupied = 0;
        for j in 0..self.positions.len() {
            let mut row = Vec::new();
            for i in 0..self.positions[j].len() {
                row.push(match self.positions[j][i] {
                    LayoutPosition::Floor => LayoutPosition::Floor,
                    LayoutPosition::Empty => if self.seats_adjacent(i, j) == 0 { changed = true; occupied += 1; LayoutPosition::Occupied } else { LayoutPosition::Empty }
                    LayoutPosition::Occupied => if self.seats_adjacent(i, j) >= 4 { changed = true; LayoutPosition::Empty } else { occupied += 1; LayoutPosition::Occupied }
                });
            }
            new_positions.push(row);
        }
        self.positions = new_positions;
        (changed, occupied)
    }

    pub fn iterate_visible(&mut self) -> (bool, usize) {
        let mut new_positions: Vec<Vec<LayoutPosition>> = Vec::new();
        let mut changed = false;
        let mut occupied = 0;
        for j in 0..self.positions.len() {
            let mut row = Vec::new();
            for i in 0..self.positions[j].len() {
                row.push(match self.positions[j][i] {
                    LayoutPosition::Floor => LayoutPosition::Floor,
                    LayoutPosition::Empty => if self.seats_visible(i, j) == 0 { changed = true; occupied += 1; LayoutPosition::Occupied } else { LayoutPosition::Empty }
                    LayoutPosition::Occupied => if self.seats_visible(i, j) >= 5 { changed = true; LayoutPosition::Empty } else { occupied += 1; LayoutPosition::Occupied }
                });
            }
            new_positions.push(row);
        }
        self.positions = new_positions;
        (changed, occupied)
    }

    pub fn print(&self) {
        print!("{esc}[1;1H", esc = 27 as char);
        for j in 0..self.positions.len() {
            for i in 0..self.positions[j].len() {
                print!("{}", self.positions[j][i]);
            }
            println!("");
        }
    }
}
