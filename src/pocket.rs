use fasthash::sea::Hasher64;
use std::{
    cmp::{
        max,
        min,
    },
    collections::HashSet,
};
use thiserror::Error;

type Hash64 = std::hash::BuildHasherDefault<Hasher64>;
type FastSet<K> = HashSet<K, Hash64>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Invalid Cube {0}")]
    InvalidCube(char),

    #[error(transparent)]
    IOError(#[from] std::io::Error),
}

#[derive(Debug, Default)]
pub struct Pocket3D {
    active_cubes: FastSet<(isize, isize, isize)>,
    i_bounds: (isize, isize),
    j_bounds: (isize, isize),
    k_bounds: (isize, isize),
}

impl Pocket3D {
    pub fn from_lines(lines: &mut impl std::iter::Iterator<Item = std::io::Result<String>>) -> Result<Self, Error> {
        let mut active_cubes: FastSet<(isize, isize, isize)> = Default::default();
        let mut i_min = isize::max_value();
        let mut i_max = isize::min_value();
        let mut j_min = isize::max_value();
        let mut j_max = isize::min_value();
        for (j, line) in lines.enumerate() {
            for (i, cube) in line?.chars().enumerate() {
                match cube {
                    '#' => {
                        let i = i as isize;
                        let j = j as isize;
                        active_cubes.insert((i, j, 0));
                        i_min = min(i_min, i);
                        i_max = max(i_max, i);
                        j_min = min(j_min, j);
                        j_max = max(j_max, j);
                    },
                    '.' => {},
                    c => return Err(Error::InvalidCube(c)),
                }
            }
        }
        Ok(Self{
            active_cubes: active_cubes,
            i_bounds: (i_min, i_max),
            j_bounds: (j_min, j_max),
            k_bounds: (0, 0),
        })
    }

    fn active_neighbours(&self, i: isize, j: isize, k: isize) -> usize {
        let mut count = 0;
        for dk in -1..=1 {
            for dj in -1..=1 {
                for di in -1..=1 {
                    if di == 0 && dj == 0 && dk == 0 {
                        continue
                    }
                    let (ii, jj, kk) = (i + di, j + dj, k + dk);
                    if self.active_cubes.contains(&(ii, jj, kk)) {
                        count += 1
                    }
                }
            }
        }
        count
    }

    pub fn iterate(&mut self) {
        let (i_min, i_max) = self.i_bounds;
        let (j_min, j_max) = self.j_bounds;
        let (k_min, k_max) = self.k_bounds;
        let mut new_i_min = isize::max_value();
        let mut new_i_max = isize::min_value();
        let mut new_j_min = isize::max_value();
        let mut new_j_max = isize::min_value();
        let mut new_k_min = isize::max_value();
        let mut new_k_max = isize::min_value();
        let mut new_active_cubes: FastSet<(isize, isize, isize)> = Default::default();
        for k in k_min - 1..=k_max + 1 {
            for j in j_min - 1..=j_max + 1 {
                for i in i_min - 1..=i_max + 1 {
                    let active_neighbours = self.active_neighbours(i, j, k);
                    match self.active_cubes.contains(&(i, j, k)) {
                        true => {
                            if active_neighbours == 2 || active_neighbours == 3 {
                                new_active_cubes.insert((i, j, k));
                                new_i_min = min(new_i_min, i);
                                new_i_max = max(new_i_max, i);
                                new_j_min = min(new_j_min, j);
                                new_j_max = max(new_j_max, j);
                                new_k_min = min(new_k_min, k);
                                new_k_max = max(new_k_max, k);
                            }
                        },
                        false => {
                            if active_neighbours == 3 {
                                new_active_cubes.insert((i, j, k));
                                new_i_min = min(new_i_min, i);
                                new_i_max = max(new_i_max, i);
                                new_j_min = min(new_j_min, j);
                                new_j_max = max(new_j_max, j);
                                new_k_min = min(new_k_min, k);
                                new_k_max = max(new_k_max, k);
                            }
                        },
                    }
                }
            }
        }
        self.active_cubes = new_active_cubes;
        self.i_bounds = (new_i_min, new_i_max);
        self.j_bounds = (new_j_min, new_j_max);
        self.k_bounds = (new_k_min, new_k_max);
    }

    pub fn active_count(&self) -> usize {
        self.active_cubes.len()
    }

    pub fn print(&self) {
        let (i_min, i_max) = self.i_bounds;
        let (j_min, j_max) = self.j_bounds;
        let (k_min, k_max) = self.k_bounds;
        for k in k_min..=k_max {
            println!("z={}", k);
            for j in j_min..=j_max {
                for i in i_min..=i_max {
                    print!("{}", if self.active_cubes.contains(&(i, j, k)) { '#' } else { '.' })
                }
                println!("")
            }
        }
    }
}

#[derive(Debug, Default)]
pub struct Pocket4D {
    active_cubes: FastSet<(isize, isize, isize, isize)>,
    i_bounds: (isize, isize),
    j_bounds: (isize, isize),
    k_bounds: (isize, isize),
    l_bounds: (isize, isize),
}

impl Pocket4D {
    pub fn from_lines(lines: &mut impl std::iter::Iterator<Item = std::io::Result<String>>) -> Result<Self, Error> {
        let mut active_cubes: FastSet<(isize, isize, isize, isize)> = Default::default();
        let mut i_min = isize::max_value();
        let mut i_max = isize::min_value();
        let mut j_min = isize::max_value();
        let mut j_max = isize::min_value();
        for (j, line) in lines.enumerate() {
            for (i, cube) in line?.chars().enumerate() {
                match cube {
                    '#' => {
                        let i = i as isize;
                        let j = j as isize;
                        active_cubes.insert((i, j, 0, 0));
                        i_min = min(i_min, i);
                        i_max = max(i_max, i);
                        j_min = min(j_min, j);
                        j_max = max(j_max, j);
                    },
                    '.' => {},
                    c => return Err(Error::InvalidCube(c)),
                }
            }
        }
        Ok(Self{
            active_cubes: active_cubes,
            i_bounds: (i_min, i_max),
            j_bounds: (j_min, j_max),
            k_bounds: (0, 0),
            l_bounds: (0, 0),
        })
    }

    fn active_neighbours(&self, i: isize, j: isize, k: isize, l: isize) -> usize {
        let mut count = 0;
        for dl in -1..=1 {
            for dk in -1..=1 {
                for dj in -1..=1 {
                    for di in -1..=1 {
                        if di == 0 && dj == 0 && dk == 0 && dl == 0 {
                            continue
                        }
                        let (ii, jj, kk, ll) = (i + di, j + dj, k + dk, l + dl);
                        if self.active_cubes.contains(&(ii, jj, kk, ll)) {
                            count += 1
                        }
                    }
                }
            }
        }
        count
    }

    pub fn iterate(&mut self) {
        let (i_min, i_max) = self.i_bounds;
        let (j_min, j_max) = self.j_bounds;
        let (k_min, k_max) = self.k_bounds;
        let (l_min, l_max) = self.l_bounds;
        let mut new_i_min = isize::max_value();
        let mut new_i_max = isize::min_value();
        let mut new_j_min = isize::max_value();
        let mut new_j_max = isize::min_value();
        let mut new_k_min = isize::max_value();
        let mut new_k_max = isize::min_value();
        let mut new_l_min = isize::max_value();
        let mut new_l_max = isize::min_value();
        let mut new_active_cubes: FastSet<(isize, isize, isize, isize)> = Default::default();
        for l in l_min - 1..=l_max + 1 {
            for k in k_min - 1..=k_max + 1 {
                for j in j_min - 1..=j_max + 1 {
                    for i in i_min - 1..=i_max + 1 {
                        let active_neighbours = self.active_neighbours(i, j, k, l);
                        match self.active_cubes.contains(&(i, j, k, l)) {
                            true => {
                                if active_neighbours == 2 || active_neighbours == 3 {
                                    new_active_cubes.insert((i, j, k, l));
                                    new_i_min = min(new_i_min, i);
                                    new_i_max = max(new_i_max, i);
                                    new_j_min = min(new_j_min, j);
                                    new_j_max = max(new_j_max, j);
                                    new_k_min = min(new_k_min, k);
                                    new_k_max = max(new_k_max, k);
                                    new_l_min = min(new_l_min, l);
                                    new_l_max = max(new_l_max, l);
                                }
                            },
                            false => {
                                if active_neighbours == 3 {
                                    new_active_cubes.insert((i, j, k, l));
                                    new_i_min = min(new_i_min, i);
                                    new_i_max = max(new_i_max, i);
                                    new_j_min = min(new_j_min, j);
                                    new_j_max = max(new_j_max, j);
                                    new_k_min = min(new_k_min, k);
                                    new_k_max = max(new_k_max, k);
                                    new_l_min = min(new_l_min, l);
                                    new_l_max = max(new_l_max, l);
                                }
                            },
                        }
                    }
                }
            }
        }
        self.active_cubes = new_active_cubes;
        self.i_bounds = (new_i_min, new_i_max);
        self.j_bounds = (new_j_min, new_j_max);
        self.k_bounds = (new_k_min, new_k_max);
        self.l_bounds = (new_l_min, new_l_max);
    }

    pub fn active_count(&self) -> usize {
        self.active_cubes.len()
    }

    pub fn print(&self) {
        let (i_min, i_max) = self.i_bounds;
        let (j_min, j_max) = self.j_bounds;
        let (k_min, k_max) = self.k_bounds;
        let (l_min, l_max) = self.l_bounds;
        for l in l_min..=l_max {
            for k in k_min..=k_max {
                println!("z={} w={}", k, l);
                for j in j_min..=j_max {
                    for i in i_min..=i_max {
                        print!("{}", if self.active_cubes.contains(&(i, j, k, l)) { '#' } else { '.' })
                    }
                    println!("")
                }
            }
        }
    }
}
