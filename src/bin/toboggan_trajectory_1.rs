use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let f = File::open("inputs/tobbogan_trajectory")?;
    let reader = BufReader::new(f);

    let mut counter = 0;
    let mut position = 0;

    let mut lines = reader.lines().map(|l| l.unwrap()).peekable();

    let first = lines.peek().unwrap();
    let length = first.len();

    for line in lines {
        if line.chars().nth(position).unwrap() == '#' {
            counter += 1;
        }
        position += 3;
        position %= length;
    }

    println!("{:?}", counter);

    Ok(())
}
