use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::maths;

fn main() -> Result<()> {
    let f = File::open("inputs/operation_order")?;
    let reader = BufReader::new(f);

    let lines = reader.lines();

    let mut accumulator = 0;

    for line in lines {
        accumulator += maths::execute_line_af(line?.as_str())?;
    }

    println!("{}", accumulator);
    Ok(())
}
