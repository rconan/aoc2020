use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::cards::RecursiveGame;

fn main() -> Result<()> {
    let f = File::open("inputs/crab_combat")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines().map(|l| l.unwrap());

    let mut game = RecursiveGame::from_lines(&mut lines)?;
    let winner = game.play_to_finish();
    println!("{}", game.score(winner));

    Ok(())
}
