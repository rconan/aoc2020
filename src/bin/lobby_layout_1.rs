use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::floor::{
    Floor,
    Tile,
};

fn main() -> Result<()> {
    let f = File::open("inputs/lobby_layout")?;
    let reader = BufReader::new(f);

    let lines = reader.lines();

    let mut floor: Floor = Default::default();

    for line in lines {
        floor.flip_tile(&Tile::from_str(line?.as_str())?);
    }

    println!("{}", floor.black_tile_count());

    Ok(())
}
