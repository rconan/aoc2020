use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::food::AllergenFilter;

fn main() -> Result<()> {
    let f = File::open("inputs/allergen_assessment")?;
    let reader = BufReader::new(f);

    let lines = reader.lines();

    let mut filter = AllergenFilter::from_lines(&mut lines.map(|l| l.unwrap()))?;
    filter.create_allergen_map()?;
    
    println!("{}", filter.dangerous_ingredients_list());

    Ok(())
}
