use anyhow::Result;
use itertools::sorted;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> Result<()> {
    let f = File::open("inputs/adapter_array")?;
    let reader = BufReader::new(f);

    let numbers = reader.lines().map(|l| l.unwrap().parse::<u32>().unwrap());

    let mut reference = 0;

    let mut distribution: [usize; 3] = [0; 3];

    for number in sorted(numbers) {
        let difference = number - reference;
        if difference > 3 {
            break
        }
        distribution[difference as usize - 1] += 1;
        reference = number;
    }

    distribution[2] += 1;

    println!("{}", distribution[0] * distribution[2]);

    Ok(())
}

