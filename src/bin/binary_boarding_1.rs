use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::seat::Seat;

fn main() -> Result<()> {
    let f = File::open("inputs/binary_boarding")?;
    let reader = BufReader::new(f);

    let lines = reader.lines().map(|l| l.unwrap());

    let mut highest = 0;
    for line in lines {
        let seat = Seat::from_string(line.as_str())?;
        let id = seat.id();
        if id > highest {
            highest = id
        }
    }
    println!("{:?}", highest);
    Ok(())
}
