use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> Result<()> {
    let f = File::open("inputs/combo_breaker")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();
    let card_public_key = lines.next().unwrap()?.parse::<u128>()?;
    let door_public_key = lines.next().unwrap()?.parse::<u128>()?;

    let mut value = 1;
    let subject = 7;
    let mut card_loop_count = 0;
    for i in 1.. {
        value *= subject;
        value %= 20201227;
        if value == card_public_key {
            card_loop_count = i;
            break
        }
    }
    println!("Card loop count: {}", card_loop_count);

    let mut encryption_key: u128 = 1;
    for _ in 0..card_loop_count {
        encryption_key *= door_public_key;
        encryption_key %= 20201227;
    }

    println!("{}", encryption_key);

    Ok(())
}
