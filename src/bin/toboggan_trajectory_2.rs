use std::{
    fs::File,
    io::{BufRead, BufReader},
};

struct Slope {
    right: usize,
    down: usize,
}

fn main() -> std::io::Result<()> {
    let f = File::open("inputs/tobbogan_trajectory")?;
    let reader = BufReader::new(f);

    let slopes = [
        Slope{right: 1, down: 1},
        Slope{right: 3, down: 1},
        Slope{right: 5, down: 1},
        Slope{right: 7, down: 1},
        Slope{right: 1, down: 2},
    ];

    let mut counters = vec![0; slopes.len()];

    let mut positions = vec![0; slopes.len()];

    let mut lines = reader.lines().map(|l| l.unwrap()).peekable();

    let mut line_no = 0;

    let first = lines.peek().unwrap();
    let length = first.len();

    for line in lines {
        for i in 0..slopes.len() {
            if line_no % slopes[i].down == 0 {
                if line.chars().nth(positions[i]).unwrap() == '#' {
                    counters[i] += 1;
                }
                positions[i] += slopes[i].right;
                positions[i] %= length;
            }
        }
        line_no += 1;
    }

    let mut product: u64 = 1;
    for counter in counters {
        product *= counter;
    }
    println!("{:?}", product);

    Ok(())
}
