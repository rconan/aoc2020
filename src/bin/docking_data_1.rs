use anyhow::Result;
use std::fs;
use aoc2020::docking::{
    Machine,
    Program,
};

fn main() -> Result<()> {
    let input = fs::read_to_string("inputs/docking_data")?;
    let program = Program::from_string(&input)?;

    let mut machine = Machine::new(&program);
    let result = machine.run()?;

    println!("{}", result);

    Ok(())
}
