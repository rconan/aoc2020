use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::cards::Game;

fn main() -> Result<()> {
    let f = File::open("inputs/crab_combat")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines().map(|l| l.unwrap());

    let mut game = Game::from_lines(&mut lines)?;
    while game.play_turn() {}
    println!("{}", game.score());

    Ok(())
}
