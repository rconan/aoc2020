use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::handheld::{
    HandheldError,
    Machine,
    Program,
};

fn main() -> Result<()> {
    let f = File::open("inputs/handheld_halting")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();
    let program = Program::from_lines(&mut lines)?;

    let mut machine = Machine::new(&program);

    if let Err(HandheldError::LoopDetected(_)) = machine.run() {
        println!("{:?}", machine.accumulator());
    }

    Ok(())
}
