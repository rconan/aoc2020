use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::password::PasswordSpec;

fn main() -> Result<()> {
    let f = File::open("inputs/password_philosophy")?;
    let reader = BufReader::new(f);

    let mut counter = 0;

    for line in reader.lines().map(|l| l.unwrap()) {
        let spec = PasswordSpec::from_line(line)?;
        if spec.new_is_valid() {
            counter += 1
        }
    }

    println!("{:?}", counter);

    Ok(())
}
