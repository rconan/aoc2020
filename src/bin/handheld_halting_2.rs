use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::handheld::{
    Machine,
    Program,
};

fn main() -> Result<()> {
    let f = File::open("inputs/handheld_halting")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();
    let program = Program::from_lines(&mut lines)?;

    let mut machine = Machine::new(&program);

    println!("{:?}", machine.run_fault_find()?);

    Ok(())
}
