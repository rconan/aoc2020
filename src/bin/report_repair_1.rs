use std::{
    collections::HashSet,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let f = File::open("inputs/report_repair")?;
    let reader = BufReader::new(f);

    let mut set: HashSet<i32> = HashSet::new();

    for line in reader.lines().map(|l| l.unwrap()) {
        let int = line.parse::<i32>().unwrap();
        let required = 2020 - int;
        if set.contains(&required) {
            println!("{:?}", int * required);
            return Ok(())
        }
        set.insert(int);
    }

    Ok(())
}
