use anyhow::Result;
use std::{
    collections::BTreeMap,
    fs::File,
    io::{BufRead, BufReader},
};

//function extended_gcd(a, b)
//    s := 0;    old_s := 1
//    r := b;    old_r := a
//         
//    while r ≠ 0 do
//        quotient := old_r div r
//        (old_r, r) := (r, old_r − quotient × r)
//        (old_s, s) := (s, old_s − quotient × s)
//    
//    if b ≠ 0 then
//        bezout_t := quotient(old_r − old_s × a, b)
//    else
//        bezout_t := 0
//    
//    output "Bézout coefficients:", (old_s, bezout_t)
//    output "greatest common divisor:", old_r

fn main() -> Result<()> {
    let f = File::open("inputs/shuttle_search")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines().map(|l| l.unwrap());

    lines.next();

    let mut map: BTreeMap<usize, usize> = BTreeMap::new();

    let line = lines.next().unwrap();
    for (a, n) in line.split(',').enumerate().filter_map(|(a, s)| match s.parse::<usize>() { Ok(n) => Some((a, n)), Err(_) => None }) {
        map.insert(n, (-(a as isize)).rem_euclid(n as isize) as usize);
    }

    let mut iter = map.iter().rev();
    let (n, a) = iter.next().unwrap();
    let mut increment = *n;
    let mut candidate = *a;
    for (n, a) in iter {
        println!("Considering new congruence: n={}, a={}", n, a); 
        let mut count = 0;
        while candidate % n != *a {
            count += 1;
            candidate += increment
        }
        increment *= n;
        println!("Solved at {} with {} iterations", candidate, count); 
    }

    println!("{}", candidate);

    Ok(())
}
