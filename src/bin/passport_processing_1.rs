use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::passport::Passport;

fn main() -> Result<()> {
    let f = File::open("inputs/passport_processing")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines().peekable();

    let mut counter = 0;

    loop {
        if let Some(_) = lines.peek() {
            let passport = Passport::from_lines(&mut lines)?;
            if passport.is_complete() {
                counter += 1
            } else {
            }
            continue
        }
        break
    }
    println!("{:?}", counter);
    Ok(())
}
