use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::food::AllergenFilter;

fn main() -> Result<()> {
    let f = File::open("inputs/allergen_assessment")?;
    let reader = BufReader::new(f);

    let lines = reader.lines();

    let filter = AllergenFilter::from_lines(&mut lines.map(|l| l.unwrap()))?;
    
    println!("{}", filter.safe_ingredients_count());

    Ok(())
}
