use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::baggage::BaggageRules;

fn main() -> Result<()> {
    let f = File::open("inputs/handy_haversacks")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();

    let rules = BaggageRules::from_lines(&mut lines)?;
    let count = rules.count_bags_contained_by("shiny gold");
    println!("{:?}", count);
    Ok(())
}
