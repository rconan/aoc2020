use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::seat::SeatLayout;

fn main() -> Result<()> {
    let f = File::open("inputs/seating_system")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();

    let mut layout = SeatLayout::from_lines(&mut lines)?;
    let mut changed = true;
    let mut occupied = 0;
    while changed {
        let (new_changed, new_occupied) = layout.iterate_visible();
        changed = new_changed;
        occupied = new_occupied;
    }
    println!("{}", occupied);
    Ok(())
}
