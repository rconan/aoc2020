use anyhow::Result;
use aoc2020::cups::Game;

fn main() -> Result<()> {
    let mut game = Game::from_str("123487596")?;
    game.append_sequential(1_000_000);
    for _ in 1..=10_000_000 {
        game.take_turn();
    }
    println!("{}", game.multiply_following_one());
    Ok(())
}
