use anyhow::Result;
use aoc2020::cups::Game;

fn main() -> Result<()> {
    let mut game = Game::from_str("123487596")?;
    for _ in 1..=100 {
        game.take_turn();
    }
    game.print_order();
    Ok(())
}
