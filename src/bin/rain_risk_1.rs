use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use aoc2020::boat::{
    Boat,
    Command,
};

fn main() -> Result<()> {
    let f = File::open("inputs/rain_risk")?;
    let reader = BufReader::new(f);

    let lines = reader.lines().map(|l| l.unwrap());

    let mut boat = Boat::new();

    for line in lines {
        let cmd = Command::from_line(line)?;
        boat.execute(cmd);
    }

    println!("{}", boat.distance());

    Ok(())
}

