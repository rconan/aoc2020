use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::camera::Array;

fn main() -> Result<()> {
    let f = File::open("inputs/jurassic_jigsaw")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();

    let array = Array::from_lines(&mut lines)?;

    let candidates = array.candidate_corner_tiles();

    if candidates.len() == 4 {
        println!("{}", candidates.iter().product::<usize>());
    }

    Ok(())
}
