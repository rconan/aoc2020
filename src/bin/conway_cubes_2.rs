use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::pocket::Pocket4D;

fn main() -> Result<()> {
    let f = File::open("inputs/conway_cubes")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();

    let mut pocket = Pocket4D::from_lines(&mut lines)?;
    for _ in 0..6 {
        pocket.iterate();
    }
    println!("{}", pocket.active_count());
    Ok(())
}
