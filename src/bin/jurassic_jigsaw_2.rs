use anyhow::Result;
use std::{
    cmp::max,
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::camera::Array;

fn main() -> Result<()> {
    let f = File::open("inputs/jurassic_jigsaw")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();

    let array = Array::from_lines(&mut lines)?;

    let image = array.full_image();
    image.print();
    let mut most_monsters = usize::min_value();
    for i in 0..8 {
        most_monsters = max(most_monsters, image.edge_to_left(i).count_monsters());
    }
    println!("{}", image.count_pixels() - most_monsters * 15);

    Ok(())
}
