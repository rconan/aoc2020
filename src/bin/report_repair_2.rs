use std::{
    collections::{HashSet, HashMap},
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> std::io::Result<()> {
    let f = File::open("inputs/report_repair")?;
    let reader = BufReader::new(f);

    let target = 2020;

    let mut set: HashSet<i32> = HashSet::new();
    let mut map: HashMap<i32, (i32, i32)> = HashMap::new();

    for line in reader.lines().map(|l| l.unwrap()) {
        let int = line.parse::<i32>().unwrap();
        let required = target - int;
        if let Some(value) = map.get(&required) {
            println!("{:?}", int * value.0 * value.1);
            return Ok(())
        }

        for val in set.iter() {
            if int + val < target {
                map.insert(int + val, (*val, int));
            }
        }
        set.insert(int);
    }

    Ok(())
}
