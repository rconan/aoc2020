use anyhow::Result;
use std::{
    collections::BTreeSet,
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::seat::Seat;

fn main() -> Result<()> {
    let f = File::open("inputs/binary_boarding")?;
    let reader = BufReader::new(f);

    let lines = reader.lines().map(|l| l.unwrap());

    let mut set = BTreeSet::new();
    for line in lines {
        let seat = Seat::from_string(line.as_str())?;
        let id = seat.id();
        set.insert(id);
    }
    for seat in &set {
        let candidate = seat + 1;
        let beyond = seat + 2;
        if !set.contains(&candidate) && set.contains(&beyond) {
            println!("{:?}", candidate);
            break
        }
    }
    Ok(())
}
