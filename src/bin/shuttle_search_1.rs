use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> Result<()> {
    let f = File::open("inputs/shuttle_search")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines().map(|l| l.unwrap());

    let time: u64 = lines.next().unwrap().parse()?;

    let mut min_id = 0;
    let mut min_wait = u64::max_value();

    for bus in lines.next().unwrap().split(',') {
        match bus {
            "x" => continue,
            s => {
                let id = s.parse()?;
                let wait = id - time % id;
                if wait < min_wait {
                    min_id = id;
                    min_wait = wait;
                }
            }
        }
    }

    println!("{}", min_id * min_wait);

    Ok(())
}
