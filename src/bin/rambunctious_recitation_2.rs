use anyhow::Result;

use aoc2020::memory::Memory;

fn main() -> Result<()> {
    let input = "12,1,16,3,11,0";
    let mut numbers = input.split(',').map(|s| s.parse::<usize>().unwrap());
    let mut memory = Memory::from_iter(&mut numbers);

    let mut val = 0;
    while memory.turn() < 30000000 {
        val = memory.next();
    }

    println!("{}", val);

    Ok(())
}
