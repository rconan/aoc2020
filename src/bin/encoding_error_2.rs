use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::xmas::{
    Decoder,
    XMASError
};

fn main() -> Result<()> {
    let f = File::open("inputs/encoding_error")?;
    let reader = BufReader::new(f);

    let mut numbers = reader.lines().map(|l| l.unwrap().parse::<u64>().unwrap());

    let mut decoder = Decoder::from_iter(25, &mut numbers)?;
    
    for number in numbers.by_ref() {
        if let Err(XMASError::InvalidItem) = decoder.decode_next(number) {
            println!("Target is: {:?}", number);
            if let Ok(weakness) = decoder.into_weakness(&mut numbers, number) {
                println!("{:?}", weakness)
            }
            break
        }
    }

    Ok(())
}
