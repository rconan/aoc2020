use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::customs::CustomsDeclaration;

fn main() -> Result<()> {
    let f = File::open("inputs/custom_customs")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines().peekable();

    let mut sum = 0;

    loop {
        if let Some(_) = lines.peek() {
            let customs_declaration = CustomsDeclaration::from_lines(&mut lines)?;
            sum += customs_declaration.any_count();
            continue
        }
        break
    }
    println!("{:?}", sum);
    Ok(())
}
