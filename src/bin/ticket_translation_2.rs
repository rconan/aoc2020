use anyhow::Result;
use std::fs;
use aoc2020::train::{
    TicketData,
};

fn main() -> Result<()> {
    let input = fs::read_to_string("inputs/ticket_translation")?;
    let td = TicketData::from_str(&input)?;

    println!("{:?}", td.departure_multiple());

    Ok(())
}
