use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::message::Validator;

fn main() -> Result<()> {
    let f = File::open("inputs/monster_messages")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();

    let validator = Validator::from_lines(&mut lines)?;

    let mut accumulator = 0;

    for line in lines.map(|l| l.unwrap()) {
        if validator.validate(line.as_str())? {
            println!("{}", line);
            accumulator += 1;
        }
    }

    println!("{}", accumulator);

    Ok(())
}
