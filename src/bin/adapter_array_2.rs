use anyhow::Result;
use itertools::sorted;
use std::{
    cmp::max,
    collections::BTreeMap,
    fs::File,
    io::{BufRead, BufReader},
};

fn main() -> Result<()> {
    let f = File::open("inputs/adapter_array")?;
    let reader = BufReader::new(f);

    let numbers = reader.lines().map(|l| l.unwrap().parse::<u32>().unwrap());

    let mut possibilities: BTreeMap<u32, usize> = Default::default();
    // We have one way to make 0 Jolts, that is using the socket directly
    possibilities.insert(0, 1);

    for number in sorted(numbers) {
        let mut new_p = 0;
        let min: u32 = max(0, number as i64 - 3) as u32;
        for i in min..number {
            if let Some(old_p) = possibilities.get(&i) {
                new_p += old_p;
            }
        }
        possibilities.insert(number, new_p);
    }

    let (_, p) = possibilities.iter().next_back().unwrap();
    println!("{}", p);

    Ok(())
}

