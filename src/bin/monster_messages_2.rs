use anyhow::Result;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};
use aoc2020::message::Validator;

fn main() -> Result<()> {
    let f = File::open("inputs/monster_messages")?;
    let reader = BufReader::new(f);

    let mut lines = reader.lines();

    let mut validator = Validator::from_lines(&mut lines)?;
    validator.patch_rules();

    let mut accumulator = 0;

    for line in lines.map(|l| l.unwrap()) {
        if validator.validate(line.as_str())? {
            accumulator += 1;
        }
    }

    println!("{}", accumulator);

    Ok(())
}
