use thiserror::Error;
use pest::{
    Parser,
    iterators::{
        Pair,
        Pairs,
    },
};
use pest_derive::Parser;
use std::{
    collections::HashMap,
    num::ParseIntError,
};

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error(transparent)]
    ParseError(#[from] pest::error::Error<Rule>),

    #[error("Unexpected Token {0}")]
    UnexpectedToken(String),

    #[error("Unexpected Mask Bit {0}")]
    UnexpectedMaskBit(char),

    #[error("Invalid argument: {0}")]
    InvalidArgument(#[from] ParseIntError),

    #[error("Machine Counter Invalid: {0}")]
    MachineCounterInvalid(usize),
}

#[derive(Parser)]
#[grammar_inline = r#"
WHITESPACE = _{ " " }

program = _{ instruction ~ ("\n"+ ~ instruction)* }

instruction = _{ mask_inst | write_inst }

mask_inst = { "mask" ~ "=" ~ mask_spec }
mask_spec = @{ mask_bit+ }
mask_bit = _{ mask_dc | mask_one | mask_zero }
mask_dc = { "X" }
mask_one = { "1" }
mask_zero = { "0" }

write_inst = { "mem" ~ "[" ~ write_address ~ "]" ~ "=" ~ write_value }
write_address = @{ NUMBER+ }
write_value = @{ NUMBER+ }
"#]
struct ProgramParser;

#[derive(Debug)]
enum Instruction {
    Mask(u64, u64),
    Write(usize, u64),
}

impl Instruction {
    fn from_pair(pair: Pair<Rule>) -> Result<Self, Error> {
        match pair.as_rule() {
            Rule::mask_inst => Self::mask_from_pairs(&mut pair.into_inner()),
            Rule::write_inst => Self::write_from_pairs(&mut pair.into_inner()),
            _ => Err(Error::UnexpectedToken(pair.as_span().as_str().to_string())),
        }
    }

    fn mask_from_pairs(pairs: &mut Pairs<Rule>) -> Result<Self, Error> {
        let spec = pairs.next().unwrap();
        let mut mask = 0;
        let mut value = 0;
        for (i, bit) in spec.as_str().chars().rev().enumerate() {
            match bit {
                'X' => continue,
                '1' => {
                    mask  |= 1 << i;
                    value |= 1 << i;
                },
                '0' => {
                    mask |= 1 << i;
                },
                _ => return Err(Error::UnexpectedMaskBit(bit)),
            }
        }
        Ok(Instruction::Mask(mask, value))
    }

    fn write_from_pairs(pairs: &mut Pairs<Rule>) -> Result<Self, Error> {
        let address = pairs.next().unwrap().as_span().as_str().parse::<usize>()?;
        let value = pairs.next().unwrap().as_span().as_str().parse::<u64>()?;
        Ok(Instruction::Write(address, value))
    }
}

#[derive(Default)]
pub struct Program {
    instructions: Vec<Instruction>,
}

impl Program {
    pub fn from_string(input: &str) -> Result<Self, Error> {
        let pairs = ProgramParser::parse(Rule::program, input)?;
        let mut instructions = Vec::new();
        for pair in pairs {
            instructions.push(Instruction::from_pair(pair)?);
        }
        Ok(Program{
            instructions: instructions,
        })
    }
}

pub struct Machine<'a> {
    program: &'a Program,

    memory: HashMap<usize, u64>,

    program_counter: usize,
    mask_mask: u64,
    mask_value: u64,
}

impl<'a> Machine<'a> {
    pub fn new(program: &'a Program) -> Self {
        Machine{
            program: program,
            memory: HashMap::new(),
            program_counter: 0,
            mask_mask: 0,
            mask_value: 0,
        }
    }

    fn run_instruction(&mut self, instruction: &Instruction) -> Result<(), Error> {
        match instruction {
            Instruction::Mask(mask, value) => {
                self.mask_mask = *mask;
                self.mask_value = *value;
            }
            Instruction::Write(address, value) => {
                let masked_value = (value & !self.mask_mask) | self.mask_value;
                self.memory.insert(*address, masked_value);
            }
        }
        self.program_counter += 1;
        Ok(())
    }

    fn run_write_v2(&mut self, address: usize, value: u64, mask_mask: u64, mask_value: u64, bit_start: usize) {
        match mask_mask {
            0x0000000fffffffff => {
                let masked_address = address | mask_value as usize;
                self.memory.insert(masked_address, value);
            },
            m => {
                for i in bit_start..36 {
                    if (m >> i) & 1 == 0 {
                        let bitmask = 1 << i;
                        let new_mask = m | bitmask;
                        let new_value = mask_value & !bitmask;
                        self.run_write_v2(address & !bitmask as usize, value, new_mask, new_value, i + 1);
                        self.run_write_v2(address | bitmask as usize, value, new_mask, new_value, i + 1);
                    }
                }
            },
        }
    }

    fn run_instruction_v2(&mut self, instruction: &Instruction) -> Result<(), Error> {
        println!("Running instruction {:?}", instruction);
        match instruction {
            Instruction::Mask(mask, value) => {
                self.mask_mask = *mask;
                self.mask_value = *value;
            }
            Instruction::Write(address, value) => {
                self.run_write_v2(*address, *value, self.mask_mask, self.mask_value, 0);
            }
        }
        self.program_counter += 1;
        Ok(())
    }

    fn memory_sum(&self) -> u64 {
        let mut sum = 0;
        for (_, value) in &self.memory {
            sum += value
        }
        sum
    }

    pub fn run(&mut self) -> Result<u64, Error> {
        for instruction in self.program.instructions.iter() {
            self.run_instruction(&instruction)?
        }
        Ok(self.memory_sum())
    }

    pub fn run_v2(&mut self) -> Result<u64, Error> {
        for instruction in self.program.instructions.iter() {
            self.run_instruction_v2(&instruction)?
        }
        Ok(self.memory_sum())
    }
}
