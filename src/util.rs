use std::{
    collections::{
        hash_map::Iter,
        HashMap,
    },
    hash::Hash,
};

#[derive(Default)]
pub struct CountingSet<T> {
    map: HashMap<T, usize>,
}

impl<T> CountingSet<T> where T: Eq + Hash {
    pub fn decrement(&mut self, item: &T) -> Option<usize> {
        let value = self.map.get_mut(item)?;
        *value -= 1;
        Some(*value)
    }

    pub fn increment(&mut self, item: T) -> usize {
        match self.map.get_mut(&item) {
            Some(v) => {
                *v += 1;
                *v
            },
            None => {
                self.map.insert(item, 1);
                1
            },
        }
    }

    pub fn get(&self, item: &T) -> usize {
        match self.map.get(item) {
            Some(v) => *v,
            None => 0,
        }
    }

    pub fn iter(&self) -> Iter<T, usize> {
        self.map.iter()
    }
}
