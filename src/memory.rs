use fasthash::sea::Hasher64;
use std::collections::HashMap;

type Hash64 = std::hash::BuildHasherDefault<Hasher64>;
type FastMap<K, V> = HashMap<K, V, Hash64>;

pub struct Memory {
    history: FastMap<usize, usize>,
    last: usize,
    turn: usize,
}

impl Memory {
    pub fn from_iter(starting: &mut impl Iterator<Item = usize>) -> Self {
        let mut history: FastMap<usize, usize> = Default::default();
        let mut last = starting.next().unwrap();
        let mut turn = 0;
        for number in starting {
            history.insert(last, turn);
            last = number;
            turn += 1;
        };
        Memory{
            history: history,
            last: last,
            turn: turn,
        }
    }

    pub fn next(&mut self) -> usize {
        let next = match self.history.get(&self.last) {
            Some(turn) => self.turn - turn,
            None => 0,
        };
        self.history.insert(self.last, self.turn);
        self.turn += 1;
        self.last = next;
        next
    }

    pub fn turn(&self) -> usize {
        self.turn + 1
    }
}
