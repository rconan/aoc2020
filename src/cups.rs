use std::{
    num::ParseIntError,
};
use thiserror::Error;


#[derive(Error, Debug)]
pub enum Error {
    #[error("Error")]
    Error,

    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error("Invalid argument: {0}")]
    InvalidArgument(#[from] ParseIntError),
}

#[derive(Debug)]
pub struct Game {
    current: usize,
    cups: Vec<usize>,
}

impl Game {
    pub fn from_str(s: &str) -> Result<Self, Error> {
        let mut cups: Vec<usize> = vec![0; s.chars().count()];
        let mut cups_iter = s.chars().map(|c| c.to_string().parse::<usize>().unwrap() - 1);
        let mut last = cups_iter.next().unwrap();
        let first = last;
        for c in cups_iter {
            cups[last] = c;
            last = c;
        }
        cups[last] = first;
        Ok(Game{
            current: 0,
            cups: cups,
        })
    }

    pub fn append_sequential(&mut self, i: usize) {
        self.cups.reserve(i - self.cups.len());
        let mut last = self.current;
        for _ in 0..self.cups.len() - 1 {
            last = self.cups[last];
        }
        self.cups[last] = self.cups.len();
        for i in self.cups.len()..i - 1 {
            self.cups.push(i + 1);
        }
        self.cups.push(self.current);
    }

    pub fn take_turn(&mut self) {
        let mut picked_up: Vec<usize> = Vec::with_capacity(3);
        let mut cup = self.current;
        for _ in 0..3 {
            cup = self.cups[cup];
            picked_up.push(cup);
        }
        let mut destination = if self.current == 0 { self.cups.len() - 1 } else { self.current - 1 };
        while picked_up.contains(&destination) {
            destination = if destination == 0 { self.cups.len() - 1 } else { destination - 1 };
        }
        self.cups[self.current] = self.cups[picked_up[2]];
        self.cups[picked_up[2]] = self.cups[destination];
        self.cups[destination] = picked_up[0];
        self.current = self.cups[self.current];
    }

    pub fn multiply_following_one(&self) -> usize {
        let first = self.cups[0];
        let second = self.cups[first];
        (first + 1) * (second + 1)
    }

    pub fn print_order(&self) {
        let mut current = 0;
        while self.cups[current] != 0 {
            current = self.cups[current];
            print!("{}", current + 1);
        }
        println!();
    }
}
