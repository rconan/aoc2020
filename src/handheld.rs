use core::convert::TryInto;
use num_bigint::{
    BigInt,
    ToBigInt,
};
use std::collections::HashSet;
use std::num::ParseIntError;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum HandheldError {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error("Unknown operation: {0}")]
    UnknownOperation(String),

    #[error("Unexpected token: {0}")]
    UnexpectedTokenCount(usize),

    #[error("Accumulator overflow")]
    AccumulatorOverflow,

    #[error("Machine Counter overflow")]
    MachineCounterOverflow,

    #[error("Machine Counter invalid: {0}")]
    MachineCounterInvalid(usize),

    #[error("Invalid argument: {0}")]
    InvalidArgument(#[from] ParseIntError),

    #[error("Loop Detected at: {0}")]
    LoopDetected(usize),
}

pub enum Instruction {
    ACC(i128),
    JMP(i128),
    NOP(i128),
}

impl Instruction {
    fn from_line(line: String) -> Result<Self, HandheldError> {
        let tokens: Vec<&str> = line.split(' ').collect();
        let n_tokens = tokens.len();
        if n_tokens != 2 {
            return Err(HandheldError::UnexpectedTokenCount(n_tokens))
        }
        let argument = match tokens[1].parse() {
            Ok(a) => a,
            Err(e) => return Err(HandheldError::InvalidArgument(e)),
        };
        match tokens[0] {
            "acc" => Ok(Instruction::ACC(argument)),
            "jmp" => Ok(Instruction::JMP(argument)),
            "nop" => Ok(Instruction::NOP(argument)),
            op => Err(HandheldError::UnknownOperation(op.to_string())),
        }
    }
}

#[derive(Default)]
pub struct Program {
    instructions: Vec<Instruction>,
}

impl Program {
    pub fn from_lines(lines: &mut impl std::iter::Iterator<Item = std::io::Result<String>>) -> Result<Self, HandheldError> {
        let mut program: Program = Default::default();
        for line in lines {
            program.instructions.push(Instruction::from_line(line?)?);
        }
        Ok(program)
    }
}

#[derive(Clone)]
pub struct Machine<'a> {
    program: &'a Program,

    accumulator: i128,
    program_counter: usize,

    visits: HashSet<usize>,
}

impl<'a> Machine<'a> {
    pub fn new(program: &'a Program) -> Self {
        Machine{
            program: program,
            accumulator: 0,
            program_counter: 0,
            visits: HashSet::new(),
        }
    }

    fn run_next_instruction(&mut self) -> Result<(), HandheldError> {
        match self.program.instructions.get(self.program_counter) {
            Some(i) => {
                self.run_instruction(i)?;
            },
            None => return Err(HandheldError::MachineCounterInvalid(self.program_counter))
        }
        Ok(())
    }

    fn run_instruction(&mut self, instruction: &Instruction) -> Result<(), HandheldError> {
        match instruction {
            Instruction::ACC(arg) => {
                let new_accumulator = self.accumulator.to_bigint().unwrap() + arg;
                match new_accumulator.try_into() {
                    Ok(a) => {
                        self.accumulator = a;
                    },
                    Err(_) => return Err(HandheldError::AccumulatorOverflow),
                }
            },
            Instruction::JMP(arg) => {
                let new_program_counter: BigInt = self.program_counter.to_bigint().unwrap() - 1 + arg;
                match new_program_counter.try_into() {
                    Ok(a) => {
                        self.program_counter = a;
                    },
                    Err(_) => return Err(HandheldError::MachineCounterOverflow),
                }
            },
            Instruction::NOP(_) => {
            },
        }
        self.program_counter += 1;
        Ok(())
    }

    pub fn accumulator(&self) -> i128 {
        self.accumulator
    }

    pub fn run(&mut self) -> Result<i128, HandheldError> {
        while self.program_counter != self.program.instructions.len() {
            if self.visits.contains(&self.program_counter) {
                return Err(HandheldError::LoopDetected(self.program_counter))
            }
            self.visits.insert(self.program_counter);
            self.run_next_instruction()?;
        }
        Ok(self.accumulator)
    }

    pub fn run_fault_find(&mut self) -> Result<i128, HandheldError> {
        while self.program_counter != self.program.instructions.len() {
            if self.visits.contains(&self.program_counter) {
                return Err(HandheldError::LoopDetected(self.program_counter))
            }
            match self.program.instructions.get(self.program_counter) {
                Some(Instruction::JMP(arg)) => {
                    let mut clone = self.clone();
                    clone.run_instruction(&Instruction::NOP(*arg))?;
                    if let Ok(accumulator) = clone.run() {
                        return Ok(accumulator)
                    }
                },
                Some(Instruction::NOP(arg)) => {
                    let mut clone = self.clone();
                    clone.run_instruction(&Instruction::JMP(*arg))?;
                    if let Ok(accumulator) = clone.run() {
                        return Ok(accumulator)
                    }
                },
                Some(_) => {},
                None => {
                    return Err(HandheldError::MachineCounterInvalid(self.program_counter))
                },
            }
            self.run_next_instruction()?;
        }
        self.run()
    }
}
