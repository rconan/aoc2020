use std::{
    f64::consts::PI,
    num::ParseIntError,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum BoatError {
    #[error("No Command")]
    NoCommand,

    #[error("Invalid Command {0}")]
    InvalidCommand(char),

    #[error("Invalid Value")]
    InvalidValue(#[from] ParseIntError),
}

#[derive(Debug)]
pub enum Command {
    North(u32),
    South(u32),
    East(u32),
    West(u32),
    Left(u16),
    Right(u16),
    Forward(u32),
}

impl Command {
    pub fn from_line(line: String) -> Result<Self, BoatError> {
        let mut chars = line.chars();
        match chars.next() {
            Some('N') => Ok(Self::North(chars.as_str().parse()?)),
            Some('S') => Ok(Self::South(chars.as_str().parse()?)),
            Some('E') => Ok(Self::East(chars.as_str().parse()?)),
            Some('W') => Ok(Self::West(chars.as_str().parse()?)),
            Some('L') => Ok(Self::Left(chars.as_str().parse()?)),
            Some('R') => Ok(Self::Right(chars.as_str().parse()?)),
            Some('F') => Ok(Self::Forward(chars.as_str().parse()?)),
            Some(c) => Err(BoatError::InvalidCommand(c)),
            None => Err(BoatError::NoCommand),
        }
    }
}

pub struct Boat {
    x: i64,
    y: i64,

    dx: i64,
    dy: i64,

    facing: f64,
}

impl Default for Boat {
    fn default() -> Self {
        Boat{
            x: 0,
            y: 0,
            dx: 10,
            dy: 1,
            facing: 0.0,
        }
    }
}

impl Boat {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn execute(&mut self, cmd: Command) {
        match cmd {
            Command::North(v) => self.y += v as i64,
            Command::South(v) => self.y -= v as i64,
            Command::East(v) => self.x += v as i64,
            Command::West(v) => self.x -= v as i64,
            Command::Left(v) => self.facing = (self.facing - (v as f64).to_radians()) % (2.0 * PI),
            Command::Right(v) => self.facing = (self.facing + (v as f64).to_radians()) % (2.0 * PI),
            Command::Forward(v) => {
                self.x += v as i64 * self.facing.cos().round() as i64;
                self.y -= v as i64 * self.facing.sin().round() as i64;
            },
        }
    }

    fn rotate_waypoint(&mut self, angle: f64) {
        let sin = angle.sin();
        let cos = angle.cos();
        let dx_1 = self.dx as f64;
        let dy_1 = self.dy as f64;
        let dx_2 = cos * dx_1 - sin * dy_1;
        let dy_2 = sin * dx_1 + cos * dy_1;
        self.dx = dx_2.round() as i64;
        self.dy = dy_2.round() as i64;
    }

    pub fn execute_actual(&mut self, cmd: Command) {
        match cmd {
            Command::North(v) => self.dy += v as i64,
            Command::South(v) => self.dy -= v as i64,
            Command::East(v) => self.dx += v as i64,
            Command::West(v) => self.dx -= v as i64,
            Command::Left(v) => self.rotate_waypoint((v as f64).to_radians()),
            Command::Right(v) => self.rotate_waypoint(-(v as f64).to_radians()),
            Command::Forward(v) => {
                self.x += v as i64 * self.dx;
                self.y += v as i64 * self.dy;
            },
        }
    }

    pub fn distance(&self) -> u64 {
        (self.x.abs() + self.y.abs()) as u64
    }
}
