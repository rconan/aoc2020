use thiserror::Error;
use pest::{
	Parser,
	iterators::{
		Pair,
		Pairs,
	},
};
use pest_derive::Parser;
use std::{
    collections::HashSet,
    iter,
    num::ParseIntError,
};

#[derive(Error, Debug)]
pub enum Error {
    #[error("Invalid argument: {0}")]
    InvalidArgument(#[from] ParseIntError),

	#[error("Missing Field Name")]
	MissingFieldName,

    #[error(transparent)]
    ParseError(#[from] pest::error::Error<Rule>),

    #[error("Unexpected Token: '{0}'")]
    UnexpectedToken(String),
}

#[derive(Debug, Hash, PartialEq, Eq)]
pub struct FieldRule {
	name: String,
	bands: Vec<(u16, u16)>,
}

impl FieldRule {
	fn band_from_pairs(pairs: &mut Pairs<Rule>) -> Result<(u16, u16), Error> {
		let lower = pairs.next().unwrap().as_span().as_str().parse::<u16>()?;
		let upper = pairs.next().unwrap().as_span().as_str().parse::<u16>()?;
		Ok((lower, upper))
	}

	fn from_bound_pairs(name: &str, pairs: &mut Pairs<Rule>) -> Result<Self, Error> {
		let mut bands = Vec::new();
		for pair in pairs {
			bands.push(Self::band_from_pairs(&mut pair.into_inner())?);
		}
		Ok(FieldRule{
			name: name.to_string(),
			bands: bands,
		})
	}

    fn from_pairs(pairs: &mut Pairs<Rule>) -> Result<Self, Error> {
		match pairs.next() {
			Some(pair) => match pair.as_rule() {
				Rule::field_name => Self::from_bound_pairs(pair.as_span().as_str(), pairs),
				_ => Err(Error::UnexpectedToken(pair.as_span().as_str().to_string())),
			},
			None => Err(Error::MissingFieldName),
		}
    }

	fn valid(&self, value: u16) -> bool {
		self.bands.iter().any(|(l, u)| *l <= value && *u >= value)
	}
}

#[derive(Debug, Default)]
pub struct Ticket {
    fields: Vec<u16>,
}

impl Ticket {
    fn from_pair(pair: Pair<Rule>) -> Result<Self, Error> {
        match pair.as_rule() {
            Rule::ticket => Self::from_pairs(&mut pair.into_inner()),
            _ => Err(Error::UnexpectedToken(pair.as_span().as_str().to_string())),
        }
    }

	fn from_pairs(pairs: &mut Pairs<Rule>) -> Result<Self, Error> {
		let mut fields = Vec::new();
		for pair in pairs {
			fields.push(pair.as_span().as_str().parse::<u16>()?);
		}
		Ok(Ticket{
			fields: fields,
		})
	}
}

#[derive(Debug, Default)]
pub struct TicketData {
    field_rules: Vec<FieldRule>,
    own_ticket: Ticket,
    seen_tickets: Vec<Ticket>,
}

#[derive(Parser)]
#[grammar_inline = r#"
WHITESPACE = _{ " " }

rules = _{ SOI ~ rule ~ ("\n" ~ rule)* ~ EOI }

rule = { field_name ~ ":" ~ range ~ "or" ~ range }
field_name = @{ (LETTER | SPACE_SEPARATOR)+ }
range = { bound ~ "-" ~ bound }
bound = @{ NUMBER+ }

own_ticket = _{ SOI ~ "your ticket:" ~ "\n" ~ ticket ~ EOI }

seen_tickets = _{ SOI ~ "nearby tickets:" ~ "\n" ~ tickets ~ EOI }
tickets = _{ ticket ~ ("\n" ~ ticket)* ~ "\n"* }

ticket = { field ~ ("," ~ field)* }
field = @{ NUMBER+ }
"#]
struct TicketParser;

impl TicketData {
    pub fn from_str(input: &str) -> Result<Self, Error> {
		let mut sections = input.split("\n\n");
		let field_rules = TicketData::rules_from_str(sections.next().unwrap())?;
		let own_ticket = TicketData::own_ticket_from_str(sections.next().unwrap())?;
		let seen_tickets = TicketData::seen_tickets_from_str(sections.next().unwrap())?;
		Ok(TicketData{
			field_rules: field_rules,
			own_ticket: own_ticket,
			seen_tickets: seen_tickets,
		})
    }

	fn rules_from_str(input: &str) -> Result<Vec<FieldRule>, Error> {
		let pairs = TicketParser::parse(Rule::rules, input)?;
		let mut rules = Vec::new();
		for pair in pairs {
			let rule = match pair.as_rule() {
				Rule::rule => FieldRule::from_pairs(&mut pair.into_inner()),
				Rule::EOI => continue,
				_ => Err(Error::UnexpectedToken(pair.as_span().as_str().to_string())),
			}?;
			rules.push(rule);
		}
		Ok(rules)
	}

	fn own_ticket_from_str(input: &str) -> Result<Ticket, Error> {
		let mut pairs = TicketParser::parse(Rule::own_ticket, input)?;
		Ok(Ticket::from_pair(pairs.next().unwrap())?)
	}

	fn seen_tickets_from_str(input: &str) -> Result<Vec<Ticket>, Error> {
		let pairs = TicketParser::parse(Rule::seen_tickets, input)?;
		let mut tickets = Vec::new();
		for pair in pairs {
			let ticket = match pair.as_rule() {
				Rule::ticket => Ticket::from_pairs(&mut pair.into_inner()),
				Rule::EOI => continue,
				_ => Err(Error::UnexpectedToken(pair.as_span().as_str().to_string())),
			}?;
			tickets.push(ticket);
		}
		Ok(tickets)
	}

	fn valid_for_any(&self, value: u16) -> bool {
		self.field_rules.iter().any(|r| r.valid(value))
	}

	fn ticket_error(&self, ticket: &Ticket) -> u16 {
		ticket.fields.iter().map(|&f| if self.valid_for_any(f) { 0 } else { f }).sum()
	}

    pub fn seen_tickets_error_sum(&self) -> u32 {
        self.seen_tickets.iter().map(|t| self.ticket_error(t) as u32).sum()
    }

    pub fn possible_fields(&self) -> Vec<HashSet<&FieldRule>> {
        let all_tickets = self.seen_tickets.iter().filter(|t| self.ticket_error(t) == 0).chain(iter::once(&self.own_ticket));
        let mut possibles: Vec<HashSet<&FieldRule>> = Vec::new();
        for _ in self.field_rules.iter() {
            possibles.push(self.field_rules.iter().collect())
        }
        for ticket in all_tickets {
            for (i, field) in ticket.fields.iter().enumerate() {
                possibles[i].retain(|p| p.valid(*field));
            }
        }
        possibles
    }

    pub fn departure_multiple(&self) -> u64 {
        let mut accumulator = 1;
        let mut multiplies = 0;
        let mut possibles = self.possible_fields();
        let mut determined = HashSet::new();
        while multiplies < 6 {
            for (i, possible) in possibles.iter().enumerate() {
                if possible.len() == 1 {
                    let name = possible.iter().next().unwrap().name.as_str();
                    if !determined.contains(name) {
                        if name.len() > 10 && &name[0..10] == "departure " {
                            accumulator *= self.own_ticket.fields[i] as u64;
                            multiplies += 1;
                        }
                        determined.insert(name);
                        println!("Determined {} as field {}. Ours is {} Accumulator {} Multiplies {}", possible.iter().next().unwrap().name, i, self.own_ticket.fields[i], accumulator, multiplies);
                    }
                }
            }
            for fp in possibles.iter_mut() {
                fp.retain(|p| !determined.contains(p.name.as_str()))
            }
        }
        accumulator
    }
}
