use lazy_static::lazy_static;
use regex::{Captures, Regex};
use std::convert::TryFrom;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum PasswordError {
    #[error("Parse Error")]
    ParseError,
}

pub struct PasswordSpec {
    pub first: usize,
    pub second: usize,
    pub character: char,
    pub password: String,
}

impl TryFrom<Captures<'_>> for PasswordSpec {
    type Error = PasswordError;

    fn try_from(caps: Captures) -> Result<Self, Self::Error> {
        Ok(PasswordSpec{
            first: caps.name("first").unwrap().as_str().parse::<usize>().unwrap(),
            second: caps.name("second").unwrap().as_str().parse::<usize>().unwrap(),
            character: caps.name("character").unwrap().as_str().chars().nth(0).unwrap(),
            password: String::from(caps.name("password").unwrap().as_str()),
        })
    }
}

impl PasswordSpec {
    pub fn from_line(line: String) -> Result<PasswordSpec, PasswordError> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^(?P<first>.+)-(?P<second>.+) (?P<character>.): (?P<password>.+)$").unwrap();
        }
        match RE.captures(&line) {
            Some(parts) => PasswordSpec::try_from(parts),
            None => Err(PasswordError::ParseError),
        }
    }

    pub fn new_is_valid(&self) -> bool {
        let mut chars = self.password.chars();
        let first = chars.nth(self.first - 1).unwrap();
        let second = chars.nth(self.second - self.first - 1).unwrap();
        let ret = (first == self.character) ^ (second == self.character);
        ret
    }

    pub fn old_is_valid(&self) -> bool {
        let mut counter = 0;
        for c in self.password.chars() {
            if c == self.character {
                counter += 1;
                if counter > self.second {
                    return false
                }
            }
        }
        counter >= self.first
    }
}
