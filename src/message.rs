use thiserror::Error;
use pest::{
    Parser,
    iterators::Pair,
};
use pest_derive::Parser;
use std::{
    collections::{
        HashSet,
        HashMap,
    },
    iter,
    num::ParseIntError,
};

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error(transparent)]
    ParseError(#[from] pest::error::Error<Rule>),

    #[error("Invalid argument: {0}")]
    InvalidArgument(#[from] ParseIntError),

    #[error("Invalid Rule: {0:?} {1}")]
    InvalidRule(Rule, String),

    #[error("No Such Rule {0}")]
    NoSuchRule(usize),
}

#[derive(Parser)]
#[grammar_inline = r#"
WHITESPACE = _{ " " }

rule = _{ rule_number ~ ":" ~ rule_definition }
rule_definition = _{ rule_primitive ~ ("|" ~ rule_primitive)* }
rule_primitive = _{ literal | composition }
composition = { rule_number+ }
literal = { "\"" ~ character ~ "\"" }
character = @{ LETTER }

rule_number = @{ NUMBER+ }
"#]
struct MessageParser;

#[derive(Debug)]
pub enum MessageRule {
    Literal(char),
    Combination(Vec<usize>),
}

impl MessageRule {
    fn from_pair(pair: Pair<Rule>) -> Result<Self, Error> {
        match pair.as_rule() {
            Rule::literal => {
                let c = pair.into_inner().next().unwrap().as_span().as_str().chars().next().unwrap();
                Ok(MessageRule::Literal(c))
            },
            Rule::composition => {
                let mut rules: Vec<usize> = Default::default();
                for pair in pair.into_inner() {
                    rules.push(pair.as_span().as_str().parse()?);
                }
                Ok(MessageRule::Combination(rules))
            },
            r => Err(Error::InvalidRule(r, pair.as_span().as_str().to_string())),
        }
    }
}

fn car_cdr(s: &str) -> (char, &str) {
    match s.chars().next() {
        Some(c) => {
            let (_, remaining) = s.split_at(c.len_utf8());
            (c, remaining)
        },
        None => ('\0', ""),
    }
}


pub struct Validator {
    rules: HashMap<usize, Vec<MessageRule>>,
}

impl Validator {
    pub fn from_lines(lines: &mut impl std::iter::Iterator<Item = std::io::Result<String>>) -> Result<Self, Error> {
        let mut rules: HashMap<usize, Vec<MessageRule>> = Default::default();
        for maybe_line in lines {
            let line = maybe_line?;
            let mut pairs = match line.as_str() {
                "" => break,
                s => MessageParser::parse(Rule::rule, s)?,
            };
            let rule_number = pairs.next().unwrap().as_span().as_str().parse()?;
            let mut rule_definitions: Vec<MessageRule> = Default::default();
            for pair in pairs {
                rule_definitions.push(MessageRule::from_pair(pair)?);
            }
            rules.insert(rule_number, rule_definitions);
        }
        Ok(Validator{
            rules: rules,
        })
    }

    fn validate_rule<'a>(&self, r: &MessageRule, s: &'a str) -> Result<(bool, HashSet<&'a str>), Error> {
        match r {
            MessageRule::Literal(c) => self.validate_literal(*c, s),
            MessageRule::Combination(rules) => self.validate_combination(rules, s),
        }
    }

    fn validate_literal<'a>(&self, c: char, s: &'a str) -> Result<(bool, HashSet<&'a str>), Error> {
        let mut tails: HashSet<&'a str> = Default::default();
        let (first, new_tail) = car_cdr(s);
        let matched = c == first;
        if matched {
            tails.insert(new_tail);
        }
        Ok((matched, tails))
    }

    fn validate_combination<'a>(&self, rules: &[usize], s: &'a str) -> Result<(bool, HashSet<&'a str>), Error> {
        match rules.split_first() {
            None => Ok((true, iter::once(s).collect())),
            Some((first_rule, remaining_rules)) => {
                let (matched, tails) = self.validate_rules(*first_rule, s)?;
                let mut new_tails: HashSet<&'a str> = Default::default();
                if !matched {
                    Ok((false, tails))
                } else {
                    for tail in tails {
                        let (m, t) = self.validate_combination(remaining_rules, tail)?;
                        if m {
                            new_tails = new_tails.union(&t).cloned().collect();
                        }
                    }
                    Ok((new_tails.len() > 0, new_tails))
                }
            },
        }
    }

    fn validate_rules<'a>(&self, n: usize, s: &'a str) -> Result<(bool, HashSet<&'a str>), Error> {
        match self.rules.get(&n) {
            Some(rules) => {
                let mut new_tails: HashSet<&'a str> = Default::default();
                for rule in rules {
                    let (matched, tails) = self.validate_rule(rule, s)?;
                    if matched {
                        new_tails = new_tails.union(&tails).cloned().collect();
                    }
                }
                Ok((new_tails.len() > 0, new_tails))
            },
            None => Err(Error::NoSuchRule(n)),
        }
    }

    pub fn patch_rules(&mut self) {
        self.rules.insert(8, vec![MessageRule::Combination(vec![42]), MessageRule::Combination(vec![42, 8])]);
        self.rules.insert(11, vec![MessageRule::Combination(vec![42, 31]), MessageRule::Combination(vec![42, 11, 31])]);
    }

    pub fn validate(&self, s: &str) -> Result<bool, Error> {
        let (matched, tails) = self.validate_rules(0, s)?;
        Ok(matched && tails.contains(""))
    }
}

#[test]
fn test_validate_rule() -> anyhow::Result<()> {
    let mut rules: HashMap<usize, Vec<MessageRule>> = Default::default();
    rules.insert(0, vec![MessageRule::Combination(vec![1]), MessageRule::Combination(vec![2])]);
    rules.insert(1, vec![MessageRule::Literal('a')]);
    rules.insert(2, vec![MessageRule::Combination(vec![1, 1])]);
    let validator = Validator{
        rules: rules,
    };
    let (matched, tails) = validator.validate_rules(0, "aa")?;
    assert!(matched);
    assert!(tails.contains("a"));
    assert!(tails.contains(""));
    Ok(())
}

#[test]
fn test_validate_combination() -> anyhow::Result<()> {
    let mut rules: HashMap<usize, Vec<MessageRule>> = Default::default();
    rules.insert(0, vec![MessageRule::Combination(vec![1, 1])]);
    rules.insert(1, vec![MessageRule::Literal('a')]);
    let validator = Validator{
        rules: rules,
    };
    let (matched, tails) = validator.validate_combination(&[1, 1], "aab")?;
    assert!(matched);
    assert_eq!(tails.len(), 1);
    assert!(tails.contains("b"));
    Ok(())
}
