use thiserror::Error;
use pest::Parser;
use pest_derive::Parser;
use std::collections::{
    HashMap,
    HashSet,
};

#[derive(Error, Debug)]
pub enum BaggageError {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error(transparent)]
    ParseError(#[from] pest::error::Error<Rule>),
}

#[derive(Default)]
pub struct BaggageRules {
    contained_by: HashMap<String, HashSet<String>>,
    contains: HashMap<String, Vec<(usize, String)>>,
}

#[derive(Parser)]
#[grammar_inline = r#"
WHITESPACE = _{ " " }

rule = _{ bag_plural ~ "contain" ~ (empty | contents) ~ "." }

contents = { bags ~ ( "," ~ bags )* }

empty = { "no other bags" }

bags = { (one ~ bag) | (number ~ bag_plural) }

bag_plural = { colour ~ "bags" }
bag = { colour ~ "bag" }

one = { "1" }
number = @{ NUMBER+ }

colour = { adjective ~ base_colour }
adjective = @{ LETTER+ }
base_colour = @{ LETTER+ }
"#]
struct BaggageParser;

impl BaggageRules {
    pub fn from_lines(lines: &mut impl std::iter::Iterator<Item = std::io::Result<String>>) -> Result<Self, BaggageError> {
        let mut rules: BaggageRules = Default::default();
        for line in lines {
            rules.parse_line(line?)?;
        }
        Ok(rules)
    }

    fn parse_line(&mut self, line: String) -> Result<(), BaggageError> {
        let mut pairs = BaggageParser::parse(Rule::rule, line.as_str())?;

        let colour = pairs.next().unwrap().into_inner().next().unwrap().as_span().as_str();
        let contents = pairs.next().unwrap();
        let mut contains: Vec<(usize, String)> = Vec::new();

        if contents.as_rule() == Rule::contents {
            for mut bags_pairs in contents.into_inner().map(|p| p.into_inner()) {
                let bag_count = bags_pairs.next().unwrap().as_span().as_str().parse().unwrap();
                let bag_colour = bags_pairs.next().unwrap().into_inner().next().unwrap().as_span().as_str();
                contains.push((bag_count, bag_colour.to_string()));
                let contained_by_key = bag_colour.to_string();
                match self.contained_by.get_mut(&contained_by_key) {
                    Some(v) => {
                        v.insert(colour.to_string());
                    },
                    None => {
                        let mut v = HashSet::new();
                        v.insert(colour.to_string());
                        self.contained_by.insert(contained_by_key, v);
                    },
                }
            }
        }

        self.contains.insert(colour.to_string(), contains);

        Ok(())
    }

    pub fn count_bags_can_contain(&self, colour: &str) -> usize {
        self.bags_including(colour).len() - 1
    }

    fn bags_including(&self, colour: &str) -> HashSet<String> {
        let mut union = HashSet::new();
        union.insert(colour.to_string());
        if let Some(set) = self.contained_by.get(colour) {
            for colour in set.iter() {
                union.extend(self.bags_including(colour));
            }
        }
        union
    }

    pub fn count_bags_contained_by(&self, colour: &str) -> usize {
        self.total_bags(colour) - 1
    }

    fn total_bags(&self, colour: &str) -> usize {
        match self.contains.get(colour) {
            Some(v) => return v.iter().map(|(n, c)| n * self.total_bags(c)).sum::<usize>() + 1,
            None => 1,
        }
    }
}
