use itertools::join;
use pest::Parser;
use pest_derive::Parser;
use std::collections::{
    BTreeMap,
    HashMap,
    HashSet,
};
use thiserror::Error;

use crate::util::CountingSet;

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    IOError(#[from] std::io::Error),

    #[error(transparent)]
    ParseError(#[from] pest::error::Error<Rule>),
}

#[derive(Parser)]
#[grammar_inline = r#"
WHITESPACE = _{ " " }

ingredients = _{ SOI ~ ingredient_list ~ allergen_list ~ EOI }

ingredient_list = { ingredient+ }
ingredient = @{ LETTER+ }

allergen_list = { "(" ~ "contains" ~ allergen ~ ("," ~ allergen)* ~ ")" }
allergen = @{ LETTER+ }
"#]
struct IngredientsParser;

#[derive(Default)]
pub struct AllergenFilter {
    possibles: HashMap<String, HashSet<String>>,
    possibles_counter: CountingSet<String>,
    total_counter: CountingSet<String>,
    allergen_map: BTreeMap<String, String>,
}

impl AllergenFilter {
    pub fn from_lines(lines: &mut impl Iterator<Item = String>) -> Result<Self, Error> {
        let mut filter: Self = Default::default();
        for line in lines {
            let mut pairs = IngredientsParser::parse(Rule::ingredients, line.as_str())?;
            let ingredients: HashSet<String> = pairs.next().unwrap().into_inner().map(|p| p.as_span().as_str().to_string()).collect();
            for ingredient in &ingredients {
                filter.total_counter.increment(ingredient.to_string());
            }
            let allergens: Vec<String> = pairs.next().unwrap().into_inner().map(|p| p.as_span().as_str().to_string()).collect();
            for allergen in allergens {
                match filter.possibles.get_mut(&allergen) {
                    Some(set) => {
                        for i in set.iter().filter(|i| !ingredients.contains(*i)) {
                            filter.possibles_counter.decrement(i).unwrap();
                        }
                        set.retain(|i| ingredients.contains(i));
                    },
                    None => {
                        filter.possibles.insert(allergen, ingredients.iter().cloned().collect());
                        for i in &ingredients {
                            filter.possibles_counter.increment(i.to_string());
                        }
                    },
                }
            }
        }
        Ok(filter)
    }

    pub fn safe_ingredients_count(&self) -> usize {
        let mut accumulator = 0;
        for (ingredient, count) in self.total_counter.iter() {
            if self.possibles_counter.get(ingredient) == 0 {
                accumulator += count;
            }
        }
        accumulator
    }

    pub fn create_allergen_map(&mut self) -> Result<(), Error> {
        while self.possibles.len() > 0 {
            let mut new_links: Vec<(String, String)> = Default::default();
            for (allergen, possibles) in self.possibles.iter() {
                if possibles.len() == 1 {
                    let ingredient = self.possibles[allergen].iter().next().unwrap();
                    new_links.push((allergen.to_string(), ingredient.to_string()));
                }
            }
            for (allergen, ingredient) in new_links {
                self.possibles.remove(&allergen);
                for (_, p) in self.possibles.iter_mut() {
                    p.remove(&ingredient);
                }
                self.allergen_map.insert(allergen, ingredient);
            }
        }
        Ok(())
    }

    pub fn dangerous_ingredients_list(&self) -> String {
        join(self.allergen_map.values(), ",")
    }
}
