use impl_ops::*;
use std::{
    collections::HashSet,
    ops,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
}

#[derive(Clone, Copy, Debug, Default, Hash, Eq, PartialEq)]
pub struct Tile {
    grid: bool,
    row: isize,
    column: isize,
}

impl_op_ex!(+ |a: &Tile, b: &Tile| -> Tile {
    Tile {
        grid: a.grid ^ b.grid,
        row: a.row + b.row + (a.grid & b.grid) as isize,
        column: a.column + b.column + (a.grid & b.grid) as isize,
    }
});

impl_op_ex!(+= |a: &mut Tile, b: &Tile| {
    let dg = (a.grid & b.grid) as isize;
    a.grid ^= b.grid;
    a.row += b.row + dg;
    a.column += b.column + dg;
});

static UNIT_VECTORS: [Tile; 6] = [
    Tile{
        grid: true,
        row: -1,
        column: 0,
    },
    Tile{
        grid: false,
        row: 0,
        column: 1,
    },
    Tile{
        grid: true,
        row: 0,
        column: 0,
    },
    Tile{
        grid: true,
        row: 0,
        column: -1,
    },
    Tile{
        grid: false,
        row: 0,
        column: -1,
    },
    Tile{
        grid: true,
        row: -1,
        column: -1,
    },
];

impl Tile {
    pub fn from_str(s: &str) -> Result<Self, Error> {
        let mut tile: Self = Default::default();
        let mut ns = '\0';
        for c in s.chars() {
            match c {
                'e' => {
                    match ns {
                        'n' => {
                            tile += UNIT_VECTORS[0];
                        },
                        '\0' => {
                            tile += UNIT_VECTORS[1];
                        },
                        's' => {
                            tile += UNIT_VECTORS[2];
                        },
                        _ => {},
                    }
                    ns = '\0';
                },
                'n' => {
                    ns = 'n';
                },
                's' => {
                    ns = 's';
                },
                'w' => {
                    match ns {
                        's' => {
                            tile += UNIT_VECTORS[3];
                        },
                        '\0' => {
                            tile += UNIT_VECTORS[4];
                        },
                        'n' => {
                            tile += UNIT_VECTORS[5];
                        },
                        _ => {},
                    }
                    ns = '\0';
                },
                _ => {},
            }
        }
        Ok(tile)
    }

    fn neighbours(&self) -> Vec<Tile> {
        UNIT_VECTORS.iter().map(|t| self + t).collect()
    }
}

#[derive(Debug, Default)]
pub struct Floor {
    black_tiles: HashSet<Tile>,
}

impl Floor {
    pub fn flip_tile(&mut self, target: &Tile) {
        match self.black_tiles.contains(target) {
            true => { self.black_tiles.remove(target); },
            false => { self.black_tiles.insert(*target); },
        }
    }

    pub fn black_tile_count(&self) -> usize {
        self.black_tiles.len()
    }

    fn count_black_neighbours(&self, tile: &Tile) -> usize {
        let mut count = 0;
        for neighbour in tile.neighbours() {
            if self.black_tiles.contains(&neighbour) {
                count += 1;
            } else {
            }
        }
        count
    }

    pub fn iterate(&mut self) {
        let mut checked: HashSet<Tile> = Default::default();
        let mut new_black_tiles: HashSet<Tile> = Default::default();
        for tile in &self.black_tiles {
            let mut black_neighbours = 0;
            for neighbour in tile.neighbours() {
                if self.black_tiles.contains(&neighbour) {
                    black_neighbours += 1;
                } else if !checked.contains(&neighbour) {
                    checked.insert(neighbour);
                    if self.count_black_neighbours(&neighbour) == 2 {
                        new_black_tiles.insert(neighbour);
                    }
                }
            }
            if black_neighbours == 1 || black_neighbours == 2 {
                new_black_tiles.insert(*tile);
            }
        }
        self.black_tiles = new_black_tiles;
    }
}
