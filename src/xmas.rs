use std::{
    collections::{
        BTreeMap,
        HashMap,
    },
    fmt::Display,
    hash::Hash,
    ops::{
        Add,
        AddAssign,
        SubAssign,
    },
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum XMASError {
    #[error("Invalid Item")]
    InvalidItem,

    #[error("No weakness found")]
    NoWeakness,
}

pub trait Decodable: Add<Output = Self> + AddAssign + Copy + Display + Default + Eq + Hash + Ord + SubAssign {}
impl<T> Decodable for T where T: Add<Output = Self> + AddAssign + Copy + Display + Default + Eq + Hash + Ord + SubAssign {}

#[derive(Default)]
pub struct Decoder<T: Decodable> {
    all: Vec<T>,

    recent_start: usize,

    valid: HashMap<T, usize>,
}

impl<T: Decodable> Decoder<T> {
    pub fn from_iter(length: usize, iter: &mut impl Iterator<Item = T>) -> Result<Self, XMASError> where T: Default {
        let mut decoder: Decoder<T> = Default::default();
        for item in iter.take(length) {
            decoder.push_recent(item)
        }
        Ok(decoder)
    }

    pub fn decode_next(&mut self, new: T) -> Result<(), XMASError> {
        if !self.valid.contains_key(&new){
            return Err(XMASError::InvalidItem)
        }
        self.pop_recent();
        self.push_recent(new);
        Ok(())
    }

    pub fn into_weakness(mut self, items: &mut impl Iterator<Item = T>, target: T) -> Result<T, XMASError>{
        for item in items {
            self.all.push(item);
        }
        let mut map: BTreeMap<T, usize> = BTreeMap::new();
        let mut sum: T = Default::default();
        let mut start = 0;
        for n in self.all.iter() {
            sum += *n;
            match map.get_mut(n) {
                Some(v) => *v += 1,
                None => {
                    map.insert(*n, 1);
                },
            }
            while sum > target {
                let k = self.all[start];
                sum -= k;
                start += 1;
                let &c = map.get(&k).unwrap();
                if c == 1 {
                    map.remove(&k);
                } else {
                    *map.get_mut(&k).unwrap() -= 1;
                }
            }
            if sum == target {
                let mut i = map.iter();
                let (first, _) = i.next().unwrap();
                let (last, _) = i.next_back().unwrap();
                return Ok(*first + *last)
            }
        }
        Err(XMASError::NoWeakness)
    }

    fn push_recent(&mut self, new: T) {
        for item in self.all[self.recent_start..].iter() {
            if *item != new {
                let sum = new + *item;
                match self.valid.get_mut(&sum) {
                    Some(v) => *v += 1,
                    None => { self.valid.insert(sum, 1); },
                }
            }
        }
        self.all.push(new);
    }

    fn pop_recent(&mut self) {
        let old = self.all[self.recent_start];
        self.recent_start += 1;
        for item in self.all[self.recent_start..].iter() {
            if *item != old {
                let sum = old + *item;
                if self.valid.get(&sum) == Some(&1) {
                    self.valid.remove(&sum).unwrap();
                } else {
                    *self.valid.get_mut(&sum).unwrap() -= 1
                }
            }
        }
    }
}
